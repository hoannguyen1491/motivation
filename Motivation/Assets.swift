// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accentColor = ColorAsset(name: "AccentColor")
  internal static let alignment = ImageAsset(name: "alignment")
  internal static let editImg = ImageAsset(name: "edit_img")
  internal static let gradientCate = ImageAsset(name: "gradient_cate")
  internal static let gradientImg = ImageAsset(name: "gradient_img")
  internal static let icFont = ImageAsset(name: "ic_font")
  internal static let leftAlignment = ImageAsset(name: "left_alignment")
  internal static let rightAlignment = ImageAsset(name: "right_alignment")
  internal static let theme1 = ImageAsset(name: "theme1")
  internal static let theme2 = ImageAsset(name: "theme2")
  internal static let theme3 = ImageAsset(name: "theme3")
  internal static let theme4 = ImageAsset(name: "theme4")
  internal static let theme5 = ImageAsset(name: "theme5")
  internal static let avatar = ImageAsset(name: "avatar")
  internal static let bg = ImageAsset(name: "bg")
  internal static let bgHome = ImageAsset(name: "bg_home")
  internal static let bgIap = ImageAsset(name: "bg_iap")
  internal static let bgIntro = ImageAsset(name: "bg_intro")
  internal static let bgIntroCate = ImageAsset(name: "bg_intro_cate")
  internal static let bgReminder = ImageAsset(name: "bg_reminder")
  internal static let bgStarting = ImageAsset(name: "bg_starting")
  internal static let bgWidget = ImageAsset(name: "bg_widget")
  internal static let doubleTouch = ImageAsset(name: "double_touch")
  internal static let singleTouch = ImageAsset(name: "single_touch")
  internal static let swipeLeft = ImageAsset(name: "swipe_left")
  internal static let swipeRight = ImageAsset(name: "swipe_right")
  internal static let swipeUp = ImageAsset(name: "swipe_up")
  internal static let headerimg = ImageAsset(name: "headerimg")
  internal static let icBack = ImageAsset(name: "ic_back")
  internal static let icBackWhite = ImageAsset(name: "ic_back_white")
  internal static let icShare = ImageAsset(name: "ic_share")
  internal static let icCategory = ImageAsset(name: "ic_category")
  internal static let icCheck = ImageAsset(name: "ic_check")
  internal static let icClose = ImageAsset(name: "ic_close")
  internal static let icCrown = ImageAsset(name: "ic_crown")
  internal static let icDownloaded = ImageAsset(name: "ic_downloaded")
  internal static let icLike = ImageAsset(name: "ic_like")
  internal static let icMark = ImageAsset(name: "ic_mark")
  internal static let icMenu = ImageAsset(name: "ic_menu")
  internal static let icSetting = ImageAsset(name: "ic_setting")
  internal static let icShare1 = ImageAsset(name: "ic_share-1")
  internal static let icUnlike = ImageAsset(name: "ic_unlike")
  internal static let rightChevron = ImageAsset(name: "right_chevron")
  internal static let imgIap = ImageAsset(name: "img_iap")
  internal static let playButton = ImageAsset(name: "play_button")
  internal static let icDownload = ImageAsset(name: "ic_download")
  internal static let icReminder = ImageAsset(name: "ic_reminder")
  internal static let icVideo = ImageAsset(name: "ic_video")
  internal static let icWidget = ImageAsset(name: "ic_widget")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = Color(asset: self)

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image named \(name).")
    }
    return result
  }
}

internal extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
