//
//  WidgetExtension.swift
//  WidgetExtension
//
//  Created by TopTomsk on 19/04/2021.
//

import WidgetKit
import SwiftUI

struct Provider: TimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date())
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date())
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []

        // Generate a timeline consisting of five entries an hour apart, starting from the current date.
        let currentDate = Date()
        for hourOffset in 0 ..< 5 {
            let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
            let entry = SimpleEntry(date: entryDate)
            entries.append(entry)
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
}

struct WidgetExtensionEntryView : View {
    var entry: Provider.Entry

    var dataImage: Data {
        get {
            if let data = UserDefaults(suiteName: "group.com.hoan.Motivations.gr")?.object(forKey: "backgroundWidget") as? Data {
                return data
            }
            if let data = UIImage(named: "theme1")?.jpegData(compressionQuality: 100) {
                return data
            }
            return Data()
        }
    }
    
    var fontWidget: UIFont {
        get {
            var fontSize = 18
            if let size = UserDefaults(suiteName: "group.com.hoan.Motivations.gr")?.object(forKey: "fontsize") as? Int {
                fontSize = size
            }
            if let data = UserDefaults(suiteName: "group.com.hoan.Motivations.gr")?.object(forKey: "WidgetFont") as? String, let font = UIFont(name: data, size: CGFloat(fontSize)) {
                return font
            }
            
            return UIFont.systemFont(ofSize: CGFloat(fontSize), weight: .semibold)
        }
    }
    
    var capital: Bool {
        get {
            if let size = UserDefaults(suiteName: "group.com.hoan.Motivations.gr")?.object(forKey: "capital") as? Bool {
                return size
            }
            
            return false
        }
    }
    
    var textColor: UIColor {
        get {
            if let hex = UserDefaults(suiteName: "group.com.hoan.Motivations.gr")?.object(forKey: "textColor") as? String, let int = Int(hex, radix: 16) {
                return UIColor(rgb: int)
            }
            
            return .white
        }
    }
    
    
    var quote: String {
        get {
            if let data = UserDefaults(suiteName: "group.com.hoan.Motivations.gr")?.object(forKey: "quoteWidget") as? String {
                return data
            }
            return "Keep calm and working hard, the dream will come true"
        }
    }
    
    var alignment: TextAlignment {
        get {
            if let data = UserDefaults(suiteName: "group.com.hoan.Motivations.gr")?.object(forKey: "align") as? Int {
                switch data {
                case 0:
                    return .leading
                case 1:
                    return .center
                default:
                    return .trailing
                }
            }
            return .leading
        }
    }
    
    var body: some View {
        HStack {
            Text(capital ? quote.uppercased() : quote.lowercased().capitalizingFirstLetter()).font(Font(fontWidget as CTFont))
                .padding()
                .multilineTextAlignment(alignment)
                .minimumScaleFactor(0.5)
                .foregroundColor(Color(textColor))
                .background(Image(uiImage: UIImage(data: dataImage)!).brightness(-0.3))
        }
    }
}

@main
struct WidgetExtension: Widget {
    let kind: String = "WidgetExtension"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            WidgetExtensionEntryView(entry: entry)
        }
        .configurationDisplayName("My Widget")
        .description("This is an example widget.")
    }
}

struct WidgetExtension_Previews: PreviewProvider {
    static var previews: some View {
        WidgetExtensionEntryView(entry: SimpleEntry(date: Date()))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
extension UIColor {
    convenience init(rgb: Int) {
        self.init(rgb: rgb, alpha: 1.0)
    }

    convenience init(rgb: Int, alpha: CGFloat) {
        let red = CGFloat(rgb >> 16) / 255.0
        let green = CGFloat(rgb >> 8 & 0xff) / 255.0
        let blue = CGFloat(rgb >> 0 & 0xff) / 255.0

        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
