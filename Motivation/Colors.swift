// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSColor
  internal typealias Color = NSColor
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  internal typealias Color = UIColor
#endif

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Colors

// swiftlint:disable identifier_name line_length type_body_length
internal struct ColorName {
  internal let rgbaValue: UInt32
  internal var color: Color { return Color(named: self) }

  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#3864c7"></span>
  /// Alpha: 100% <br/> (0x3864c7ff)
  internal static let mainColor = ColorName(rgbaValue: 0x3864c7ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#76d6ff"></span>
  /// Alpha: 100% <br/> (0x76d6ffff)
  internal static let mainColor2 = ColorName(rgbaValue: 0x76d6ffff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4086f4"></span>
  /// Alpha: 100% <br/> (0x4086f4ff)
  internal static let blueGoogle = ColorName(rgbaValue: 0x4086f4ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#00be98"></span>
  /// Alpha: 100% <br/> (0x00be98ff)
  internal static let blueGradient1 = ColorName(rgbaValue: 0x00be98ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#3864c7"></span>
  /// Alpha: 100% <br/> (0x3864c7ff)
  internal static let blueGradient2 = ColorName(rgbaValue: 0x3864c7ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#c60000"></span>
  /// Alpha: 100% <br/> (0xc60000ff)
  internal static let colorError = ColorName(rgbaValue: 0xc60000ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#00a100"></span>
  /// Alpha: 100% <br/> (0x00a100ff)
  internal static let colorSuccess = ColorName(rgbaValue: 0x00a100ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#aafcff"></span>
  /// Alpha: 100% <br/> (0xaafcffff)
  internal static let colorVehicle = ColorName(rgbaValue: 0xaafcffff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#47a3ff"></span>
  /// Alpha: 100% <br/> (0x47a3ffff)
  internal static let colorVehicleBoder = ColorName(rgbaValue: 0x47a3ffff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ffd000"></span>
  /// Alpha: 100% <br/> (0xffd000ff)
  internal static let colorWarning = ColorName(rgbaValue: 0xffd000ff)
}
// swiftlint:enable identifier_name line_length type_body_length

// MARK: - Implementation Details

internal extension Color {
  convenience init(rgbaValue: UInt32) {
    let components = RGBAComponents(rgbaValue: rgbaValue).normalized
    self.init(red: components[0], green: components[1], blue: components[2], alpha: components[3])
  }
}

private struct RGBAComponents {
  let rgbaValue: UInt32

  private var shifts: [UInt32] {
    [
      rgbaValue >> 24, // red
      rgbaValue >> 16, // green
      rgbaValue >> 8,  // blue
      rgbaValue        // alpha
    ]
  }

  private var components: [CGFloat] {
    shifts.map {
      CGFloat($0 & 0xff)
    }
  }

  var normalized: [CGFloat] {
    components.map { $0 / 255.0 }
  }
}

internal extension Color {
  convenience init(named color: ColorName) {
    self.init(rgbaValue: color.rgbaValue)
  }
}
