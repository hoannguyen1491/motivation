//
//  HomeCellViewModel.swift
//  Motivation
//
//  Created by TopTomsk on 16/03/2021.
//

import Foundation
import Alamofire
import Photos

class HomeCellViewModel {
    
    // MARK: - Properties
    var error: Error? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    // MARK: - Constructor
    init() {
        
    }
    
    // MARK: - Closures for callback, since we are not using the ViewModel to the View.
    var showAlertClosure: (() -> ())?
    var didFinishDownloaded: (() -> ())?
    var progress: ((_ value: Double)->())?
    
    // MARK: - Network call - Handler
    func downloadVideo(url: URL) {
        AF.request(url.absoluteString).downloadProgress(closure : { [weak self] (progress) in
            guard let `self` = self else {return}
            self.progress?(progress.fractionCompleted)
        }).responseData { [weak self] (response) in
            guard let `self` = self else {return}
            switch response.result {
                case .success(let data):
                    let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    let videoURL = documentsURL.appendingPathComponent("video.mp4")
                    do {
                        try data.write(to: videoURL)
                        PHPhotoLibrary.shared().performChanges({
                            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoURL)
                        }) { saved, error in
                            if saved {
                                self.didFinishDownloaded?()
                            } else {
                                self.showAlertClosure?()
                            }
                        }
                    } catch {
                        self.showAlertClosure?()
                    }
                case .failure:
                    self.showAlertClosure?()
            }
        }
    }
    
}

