//
//  CategoryViewModel.swift
//  Motivation
//
//  Created by TopTomsk on 31/03/2021.
//

import Foundation
import RealmSwift

class CategoryViewModel {
    
    // MARK: - Properties
    
    var categories = [CategoryModel]()
    var categoriesVideo = [CategoryObject]()
    
    var error: Error? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    // MARK: - Constructor
    init() {
        
    }
    
    // MARK: - Closures for callback, since we are not using the ViewModel to the View.
    var showAlertClosure: (() -> ())?
    var didFinishFetched: (() -> ())?
    
    // MARK: - Network call - Handler
    func getCategories() {
        APIRequest.share.getListCategory { [weak self] (cates, error) in
            guard let `self` = self else {return}
            guard let `cates` = cates else {
                self.showAlertClosure?()
                return
            }
            self.categories = cates
            self.didFinishFetched?()
        }
    }
    
    func getCategoriesVideos() {
        
        // First: Get like video
        let group = DispatchGroup()
        group.enter()
        var likeVideo = [VideoObject]()
        let realm = try! Realm()
        let videos = realm.objects(RealmVideoLike.self)
        let likeVideos = Array(videos).compactMap {$0.data}
        let decode = JSONDecoder()
        for item in likeVideos {
            do {
                let obj = try decode.decode(VideoObject.self, from: item)
                likeVideo.append(obj)
            } catch {
            
            }
        }
        let cateLike = CategoryObject(id: -2, name: L10n.Text.yourLikes, videos: likeVideo)
        self.categoriesVideo.append(cateLike)
        group.leave()
        
        //Second: Get all video
        group.enter()
        APIRequest.share.getListVideoHome { [weak self] (videos, error) in
            guard let `self` = self else {return}
            if let videos = videos {
                let cateObj = CategoryObject(id: -1, name: L10n.Text.all, videos: videos.shuffled())
                self.categoriesVideo.append(cateObj)
                group.leave()
            } else {
                let allVideos = Constant.share.getDefaultVideo()
                if !allVideos.isEmpty {
                    let cateObj = CategoryObject(id: -1, name: L10n.Text.all, videos: Constant.share.getDefaultVideo())
                    self.categoriesVideo.append(cateObj)
                }
                group.leave()
            }
        }
        
        //Third: Get video by categories
        group.enter()
        APIRequest.share.getCategoryVideos { [weak self] (data, error) in
            guard let `self` = self else {return}
            if let `data` = data {
                do {
                    let categorys = try JSONDecoder().decode([CategoryObject].self, from: data as! Data)
                    //print(categorys)
                    self.categoriesVideo.append(contentsOf: categorys)
                } catch {
                    self.categoriesVideo.append(contentsOf: Constant.share.getDefaultDataCategory())
                }
            } else {
                self.categoriesVideo.append(contentsOf: Constant.share.getDefaultDataCategory())
            }
            group.leave()
        }
        group.notify(queue: .main) {
            self.didFinishFetched?()
        }
    }
}
