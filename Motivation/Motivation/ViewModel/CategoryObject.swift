//
//  CategoryObject.swift
//  Motivation
//
//  Created by hoanglinh on 19/03/2021.
//

import Foundation
import UIKit
import RealmSwift

struct CategoryObject: Codable {
    var category_id: Int
    var category_name: String
    var video_info: [VideoObject]
    
    init(id: Int, name: String, videos: [VideoObject]) {
        self.category_id = id
        self.category_name = name
        self.video_info = videos
    }
    
}

struct CategoryModel: Codable {
    var category_id: Int
    var category_name: String
    var del_flg: Int
    var created_at: String?
    var updated_at: String?
}

struct VideoObject: Codable, Equatable {
    var video_id: Int
    var category_id: Int
    var category_name: String
    var title: String
    var cover: String
    var url: String
    var like_number: Int
    var duration: String
    var video_size: String
    var quotes: String
    var del_flg: Int
    var created_at: String?
    var updated_at: String?
}

class RealmVideoObject: Object {
    @objc dynamic var data = Data()
}

class ThemeObject: Object {
    @objc dynamic var theme = Data()
    @objc dynamic var index = 0
}
class RealmVideoLike: Object {
    @objc dynamic var urlString = ""
    @objc dynamic var data = Data()
    
    override class func primaryKey() -> String {
        return "urlString"
    }
}

