//
//  VideosViewModel.swift
//  Motivation
//
//  Created by TopTomsk on 31/03/2021.
//

import Foundation
import RealmSwift

class VideosViewModel {
    
    // MARK: - Properties
    
    var videos = [VideoObject]()
    
    var error: Error? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    // MARK: - Constructor
    init() {
        
    }
    
    // MARK: - Closures for callback, since we are not using the ViewModel to the View.
    var showAlertClosure: (() -> ())?
    var didFinishFetched: (() -> ())?
    
    // MARK: - Network call - Handler
    func getVideos(idCate: Int) {
        if idCate >= 0 {
            //Load videos of cate where id = idcate
            APIRequest.share.getListVideoOfCategory(idCate: idCate) { [weak self] (videos, error) in
                guard let `self` = self else {return}
                guard let `videos` = videos else {
                    self.showAlertClosure?()
                    return
                }
                self.videos = videos
                self.didFinishFetched?()
            }
        } else {
            //Load videos home default
            if idCate == -1 {
                APIRequest.share.getListVideoHome { [weak self] (videos, error) in
                    guard let `self` = self else {return}
                    guard let `videos` = videos else {
                        self.showAlertClosure?()
                        return
                    }
                    self.videos = videos
                    self.didFinishFetched?()
                }
            } else {
                //Load liked videos
                var likeVideo = [VideoObject]()
                let realm = try! Realm()
                let videos = realm.objects(RealmVideoLike.self)
                let likeVideos = Array(videos).compactMap {$0.data}
                let decode = JSONDecoder()
                for item in likeVideos {
                    do {
                        let obj = try decode.decode(VideoObject.self, from: item)
                        likeVideo.append(obj)
                    } catch {
                    
                    }
                }
                if likeVideo.count > 0 {
                    self.videos = likeVideo
                    self.didFinishFetched?()
                }
            }
        }
    }
}
