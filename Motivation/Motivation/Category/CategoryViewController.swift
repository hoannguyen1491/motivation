//
//  StretchyHeaderViewController.swift
//  StretchyHeaderViewController
//

import Foundation
import UIKit
import RealmSwift

protocol CategoryViewControllerDelegate: class {
    func categoryViewController(_ viewController: UIViewController, categoryIdClick id: Int, videoID: Int)
}

class CategoryViewController: UIViewController, UITextFieldDelegate {

    weak var delegate: CategoryViewControllerDelegate?
    var isPush = false
    
    let navBar = CustomNavigationBar()

    // MARK : - Attributes
    var headerTitle: String? {
        didSet {
            titleLabel.text = headerTitle
        }
    }
    
    var headerSubtitle: String? {
        didSet {
            subtitleLabel.text = headerSubtitle
        }
    }

    var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }

    var minHeaderHeight: CGFloat = 0 {
        didSet {
            updateHeaderView()
        }
    }

    var maxHeaderHeight: CGFloat = 300 {
        didSet {
            updateHeaderView()
        }
    }

    var tintColor: UIColor = .white {
        didSet {
            titleLabel.textColor = tintColor
            subtitleLabel.textColor = tintColor
        }
    }

    var titleFont: UIFont = UIFont.boldSystemFont(ofSize: 18) {
        didSet {
            titleLabel.font = titleFont
        }
    }

    var subtitleFont: UIFont = UIFont.systemFont(ofSize: 20) {
        didSet {
            subtitleLabel.font = subtitleFont
        }
    }

    var shadowColor: CGColor = UIColor.black.cgColor {
        didSet {
            titleLabel.layer.shadowColor = shadowColor
            subtitleLabel.layer.shadowColor = shadowColor
        }
    }

    var shadowOffset: CGSize = .zero {
        didSet {
            titleLabel.layer.shadowOffset = shadowOffset
            subtitleLabel.layer.shadowOffset = shadowOffset
        }
    }

    var shadowRadius: CGFloat = 0 {
        didSet {
            titleLabel.layer.shadowRadius = shadowRadius
            subtitleLabel.layer.shadowRadius = shadowRadius
        }
    }

    var shadowOpacity: Float = 0 {
        didSet {
            titleLabel.layer.shadowOpacity = shadowOpacity
            subtitleLabel.layer.shadowOpacity = shadowOpacity
        }
    }

    var collectionViewCategory: UICollectionView = {
        let collec = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collec.keyboardDismissMode = .onDrag
        collec.register(UINib(nibName: L10n.Text.categoryHorizontalCell, bundle: nil), forCellWithReuseIdentifier: L10n.Text.categoryHorizontalCell)
        collec.register(UINib(nibName: L10n.Text.itemCateHorizontalCell, bundle: nil), forCellWithReuseIdentifier: L10n.Text.itemCateHorizontalCell)
        collec.register(UINib(nibName: L10n.Text.unlockAllCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: L10n.Text.unlockAllCollectionViewCell)
        return collec
    }()

    var progress : CGFloat {
        return (imageView.frame.height - minHeaderHeight)/(maxHeaderHeight - minHeaderHeight)
    }

    var headerCollapsingAnimationDuration: Double = 1
    var headerExpandingAnimationDuration: Double = 1

    // Defining margin in this file in order to reuser the class in multiple projects
    fileprivate let margin: CGFloat = 10

    // MARK : - UI Elements
    fileprivate lazy var imageView: GradientImageView = {
        var image = GradientImageView(frame: .zero)
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true

        return image
    }()

    fileprivate lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.font = titleFont
        titleLabel.textColor = tintColor
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.layer.shadowColor = shadowColor
        titleLabel.layer.shadowOffset = shadowOffset
        titleLabel.layer.shadowRadius = shadowRadius
        titleLabel.layer.shadowOpacity = shadowOpacity
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()

    fileprivate lazy var subtitleLabel: UITextField = {
        let subtitleLabel = UITextField()
        subtitleLabel.font = subtitleFont
        subtitleLabel.leftView = UIView(frame: .init(x: 0, y: 0, width: 16, height: 40))
        subtitleLabel.leftViewMode = .always
        subtitleLabel.textColor = tintColor
        subtitleLabel.backgroundColor = #colorLiteral(red: 0.6615243554, green: 0.6605639458, blue: 0.6766911149, alpha: 0.4811043645)
        subtitleLabel.clearButtonMode = .whileEditing
        subtitleLabel.cornerRadius = 4
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subtitleLabel.attributedPlaceholder = NSAttributedString(string: "🔍 Tìm kiếm",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        return subtitleLabel
    }()

    var filteredData: [CategoryObject] = []
    var dataCategory = [CategoryObject]()
    
    var didAnimated = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK :  - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .black

        collectionViewCategory.contentInsetAdjustmentBehavior = .never
        collectionViewCategory.backgroundColor = .clear
        collectionViewCategory.delegate = self
        collectionViewCategory.dataSource = self

        self.headerTitle = "Lorem ipsum dolor sit amet, consec adipiscing elit. "
        self.headerSubtitle = ""
        self.image = UIImage(named: Asset.headerimg.name)

        self.minHeaderHeight = 54 + (IS_IPHONE_X ? 44 : 20)
        self.maxHeaderHeight = 300
        
        setupViews()
    }
    
    var didGetALl = false

    var heightAnchorSearch: NSLayoutConstraint?
    var bottomAnchorSearch: NSLayoutConstraint?
    var leftAnchorSearch: NSLayoutConstraint?
    var rightAnchorSearch: NSLayoutConstraint?
    
    fileprivate func setupViews() {

        view.addSubview(collectionViewCategory)
        collectionViewCategory.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        view.addSubview(imageView)
        imageView.frame.size = CGSize(width: view.frame.width, height: maxHeaderHeight)

        view.bringSubviewToFront(imageView)

        view.addSubview(navBar)
        navBar.title.text = "Category"
        navBar.title.textColor = .white
        navBar.layer.zPosition = 5
        navBar.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.right.equalToSuperview()
            make.height.equalTo(44)
        }
        navBar.delegate = self

        collectionViewCategory.contentInset.top = imageView.frame.height
        collectionViewCategory.contentOffset.y = -imageView.frame.height

        subtitleLabel.delegate = self
        view.addSubview(subtitleLabel)

        bottomAnchorSearch = subtitleLabel.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -60)
        bottomAnchorSearch!.isActive = true
        leftAnchorSearch = subtitleLabel.leadingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: margin)
        leftAnchorSearch!.isActive = true
        rightAnchorSearch = subtitleLabel.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: -margin)
        rightAnchorSearch!.isActive = true

        heightAnchorSearch = subtitleLabel.heightAnchor.constraint(equalToConstant: 52)
        heightAnchorSearch!.isActive = true

        view.addSubview(titleLabel)
        titleLabel.leadingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: margin).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: -margin).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: subtitleLabel.topAnchor, constant: -margin).isActive = true

        view.addSubview(navBar)
        navBar.title.text = "Category"
        navBar.title.textColor = .white
        navBar.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.right.equalToSuperview()
            make.height.equalTo(44)
        }
        
        //Hide Keyboard
        self.view.addTapGestureRecognizer { [weak self] in
            guard let `self` = self else {return}
            self.view.endEditing(true)
        }
        
        //Check if VC is from swipe HomeVC, then add action swipe left to pop VC
        if isPush {
            if let nav = navigationController as? NavigationController {
                nav.fullWidthBackGestureRecognizer.isEnabled = false
            }
            
            view.isUserInteractionEnabled = true
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(popVCBySwipeLeft(sender:)))
            swipeLeft.direction = .left
            view.addGestureRecognizer(swipeLeft)
        }
    }
    
    @objc func popVCBySwipeLeft(sender: UIGestureRecognizer) {
        popVCByAnimationFromRightToLeft()
    }
    
    private func popVCByAnimationFromRightToLeft() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        if let nav = navigationController as? NavigationController {
            nav.fullWidthBackGestureRecognizer.isEnabled = true
        }
        self.navigationController?.popViewController(animated: false)
    
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !didAnimated {
            self.view.alpha = 0
            self.view.cornerRadius = 16
            view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            UIView.animate(withDuration: 0.1, animations: { [weak self] in
                self?.view.cornerRadius = 0
                self?.view.transform = CGAffineTransform.identity
                self?.view.alpha = 1
            })
            didAnimated = true
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    deinit {
        print("categoryViewcontroller deinit")
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateHeaderView()
    }

    func expandHeader() {
        UIView.animate(withDuration: headerExpandingAnimationDuration) {
            self.collectionViewCategory.contentOffset.y = -self.maxHeaderHeight
            self.imageView.frame.size.height = self.maxHeaderHeight
            self.view.layoutIfNeeded()
        }
    }

    func collapseHeader() {
        UIView.animate(withDuration: headerCollapsingAnimationDuration) {
            self.collectionViewCategory.contentOffset.y = -self.minHeaderHeight
            self.imageView.frame.size.height = self.minHeaderHeight
            self.view.layoutIfNeeded()
        }
    }

    func updateHeaderView() {

        if collectionViewCategory.contentOffset.y < -maxHeaderHeight {
            imageView.frame.size.height = -collectionViewCategory.contentOffset.y
        } else if collectionViewCategory.contentOffset.y >= -maxHeaderHeight && collectionViewCategory.contentOffset.y < -minHeaderHeight {
            imageView.frame.size.height = -collectionViewCategory.contentOffset.y
        } else {
            imageView.frame.size.height = minHeaderHeight
        }

        collectionViewCategory.scrollIndicatorInsets = collectionViewCategory.contentInset
        self.titleLabel.alpha = self.progress
//        subtitleLabel.alpha = progress
        self.bottomAnchorSearch?.constant = -self.margin - self.progress*50
        if self.progress <= 1 {
            self.leftAnchorSearch?.constant = self.margin + 44 - self.progress*44
            self.heightAnchorSearch?.constant = 40 + self.progress*12
        } else {
            self.leftAnchorSearch?.constant = self.margin
            self.heightAnchorSearch?.constant = 52
        }
//        rightAnchorSearch?.constant = -self.margin - 44 + self.progress*44
        self.navBar.title.alpha = self.progress

    }

    //Search textField
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == subtitleLabel {
            if textField.text?.count ?? 0 > 0 {
                for i in 0..<dataCategory.count {
                    filteredData[i].video_info = dataCategory[i].video_info.filter({ $0.quotes.lowercased().contains(subtitleLabel.text?.lowercased() ?? "")})
                }
            } else {
                filteredData = dataCategory
            }
            
            self.collectionViewCategory.reloadData()
        } else {
            print("failed")
        }
    }
    
    let model = CategoryViewModel()
    //MARK: -RequestAPI
    func getDataCategorys() {
        model.getCategoriesVideos()
        model.didFinishFetched = { [weak self] in
            guard let self = self else {return}
            self.dataCategory.append(contentsOf: self.model.categoriesVideo)
            self.filteredData = self.dataCategory
            self.collectionViewCategory.reloadData()
        }
    }

}

//MARK: CollectionView
extension CategoryViewController:  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let numberOfSection = 2
        return numberOfSection
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {
            return filteredData.count
        }
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let section = indexPath.section
        let row = indexPath.row
        if section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: L10n.Text.categoryHorizontalCell, for: indexPath) as! CategoryHorizontalCell
            if filteredData[row].category_name == L10n.Text.all {
                cell.setDataToCell(data: filteredData[row].video_info, type: 1, typeCate: .all)
                cell.didClickCate = { [weak self] (id, videoID) in
                    guard let `self` = self else {return}
                    self.delegate?.categoryViewController(self, categoryIdClick: -1, videoID: videoID)
                    self.customNavigationBar_BackClick(self.navBar)
                }
            } else if filteredData[row].category_name == L10n.Text.yourLikes {
                cell.setDataToCell(data: filteredData[row].video_info, type: 1, typeCate: .like)
                cell.didClickCate = { [weak self] (id, videoID) in
                    guard let `self` = self else {return}
                    self.delegate?.categoryViewController(self, categoryIdClick: -2, videoID: videoID)
                    self.customNavigationBar_BackClick(self.navBar)
                }
            } else {
                cell.setDataToCell(data: filteredData[row].video_info, type: 1, typeCate: .normal)
                cell.didClickCate = { [weak self] (id, videoID) in
                    guard let `self` = self else {return}
                    self.delegate?.categoryViewController(self, categoryIdClick: self.filteredData[row].category_id, videoID: videoID)
                    self.customNavigationBar_BackClick(self.navBar)
                }
            }
            return cell
        } else if section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UnlockAllCollectionViewCell", for: indexPath) as! UnlockAllCollectionViewCell
            cell.setDataToCell()
            cell.unlockAction = { [weak self] in
                guard let `self` = self else {return}
                print("Unlock Click")
            }
            return cell
        }
        
        return UICollectionViewCell.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let row = indexPath.row
        let section = indexPath.section
        
        if section == 0 {
            if filteredData[row].video_info.count > 0 {
                return CGSize(width: collectionView.frame.width , height: 350)
            }
            return CGSize(width: collectionView.frame.width , height: 0)
        }
        
        return CGSize(width: collectionView.frame.width , height: 150)
    }
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
extension CategoryViewController: CustomNavigationBarDelegate {
    @objc func customNavigationBar_BackClick(_ view: UIView) {
        if isPush {
            popVCByAnimationFromRightToLeft()
        } else {
            self.view.cornerRadius = 16
            UIView.animate(withDuration: 0.1) {
                self.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                self.view.alpha = 0
            } completion: { (_) in
                self.navigationController?.popViewController(animated: false)
            }
        }
    }
}
