//
//  CategoryHorizontalCell.swift
//  Motivation
//
//  Created by hoanglinh on 05/03/2021.
//

import UIKit

enum TypeCategory {
    case normal
    case all
    case like
}

class CategoryHorizontalCell: UICollectionViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var titleCellLabel: UILabel!
    @IBOutlet weak var cstHeightTitleCell: NSLayoutConstraint!
    
    var heightCell: CGFloat = 0
    var widthCell: CGFloat = 0
    
    var data: [VideoObject] = []
    var type: Int = 0
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    var didClickCate: ((_ id: Int, _ videoID: Int)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func setDataToCell(data: [VideoObject], type: Int, typeCate: TypeCategory) {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: L10n.Text.itemCateHorizontalCell, bundle: nil), forCellWithReuseIdentifier: L10n.Text.itemCateHorizontalCell)

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        if type != 2 {
            layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
            layout.invalidateLayout()
        }
        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .clear
        titleCellLabel.textColor = .white
        titleCellLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        if data.count > 0 {
            if typeCate == .all {
                titleCellLabel.text = L10n.Text.all
            } else if typeCate == .like {
                titleCellLabel.text = L10n.Text.yourLikes
            } else {
                titleCellLabel.text = data[0].category_name
            }
        }
        
        cstHeightTitleCell.constant = 50
        heightCell = 300
        widthCell = UIScreen.main.bounds.width*2/3 - 40
        
        if type == 3 {
            heightCell = 200
            widthCell = 200
            //cstHeightTitleCell.constant = 0
        } else if type == 2 {
            heightCell = 400
            widthCell = UIScreen.main.bounds.width
        }
        self.data = data
        self.type = type
        self.collectionView.reloadData()
    }
    
    @objc func reloadCollectionView(notification: NSNotification) {
//        if let data = notification.userInfo?["data"] as? [String] {
//            self.data = data
//        }
//
//        UIView.performWithoutAnimation {
            self.collectionView.reloadData()
//        }
    }
    
}

extension CategoryHorizontalCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: L10n.Text.itemCateHorizontalCell, for: indexPath) as! ItemCateHorizontalCell
        if data.count > 0 {
            cell.setDataToCell(data: data[indexPath.row])
        }
        
        cell.addTapGestureRecognizer { [weak self] in
            guard let `self` = self else {return}
            self.didClickCate?(self.data[indexPath.row].category_id, self.data[indexPath.row].video_id)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.widthCell, height: self.heightCell)
    }
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}
