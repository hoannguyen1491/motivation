//
//  ItemCateHorizontalCell.swift
//  Motivation
//
//  Created by hoanglinh on 05/03/2021.
//

import UIKit
import Kingfisher

class ItemCateHorizontalCell: UICollectionViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var markButton: UIButton!
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak var desLabel: VerticalTopAlignLabel!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var viewContainer: UIView!
    
    var markButtonAction: (()->())?
    
    var dataItem: VideoObject?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor.clear.cgColor, #colorLiteral(red: 0.1019607843, green: 0.1019607843, blue: 0.1019607843, alpha: 0.8966142927).cgColor]
        gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 0.2)
        gradient.frame = CGRect(x: 0, y: self.frame.height - self.frame.height*2/3 + 40, width: self.frame.width, height: self.frame.height*2/3 - 40)

        bgImageView.layer.insertSublayer(gradient, at: 0)
       
        viewContainer.backgroundColor = .black
        viewContainer.addShadow(shadowColor: .lightGray, shadowRadius: 2)
        viewContainer.cornerRadius = 8
       
    }
    
    func setDataToCell(data: VideoObject) {
        titleButton.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        titleButton.setTitle(data.title, for: .normal)
        titleButton.setTitleColor(.white, for: .normal)
        titleButton.backgroundColor = #colorLiteral(red: 0, green: 0.7803921569, blue: 0.6352941176, alpha: 1)
        titleButton.cornerRadius = titleButton.frame.height/2
        
        bgImageView.contentMode = .scaleAspectFill
        bgImageView.cornerRadius = 8
        
        desLabel.text = data.quotes.uppercased()
        desLabel.textColor = .white
        timeLabel.textColor = .white
        
        timeLabel.text = data.duration.chopPrefix(3)
        
        markButton.addTarget(self, action: #selector(markButtonDidTap), for: .touchUpInside)
        
//        bgImageView.image = UIImage(named: data.cover)
        
        if let urlString = URL(string: data.url.convertToRealURLString()) {
            HomePageCollectionViewCell.imageFromVideo(url: urlString, at: 0) { (image) in
                guard let `image` = image else {
                    DispatchQueue.main.async {
                        if urlString == URL(string: data.url.convertToRealURLString()) {
                            self.bgImageView.image = UIImage(named: Asset.theme1.name)
                        }
                    }
                    return
                }
                DispatchQueue.main.async {
                    self.bgImageView.image = image

                }
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bgImageView.image = nil
    }
    
    @objc func markButtonDidTap() {
        self.markButtonAction?()
    }

}

