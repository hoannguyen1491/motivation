//
//  CategoryHeaderView.swift
//  Motivation
//
//  Created by hoanglinh on 05/03/2021.
//

import UIKit

class CategoryHeaderView: UICollectionReusableView {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
    }
    
}
