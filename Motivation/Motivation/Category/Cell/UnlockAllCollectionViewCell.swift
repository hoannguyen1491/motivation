//
//  UnlockAllCollectionViewCell.swift
//  Motivation
//
//  Created by hoanglinh on 07/03/2021.
//

import UIKit

class UnlockAllCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var ic_crown: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    var unlockAction: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func setDataToCell() {
        titleLabel.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        titleLabel.textColor = .white
        
        containerView.backgroundColor = #colorLiteral(red: 0.2078241408, green: 0.2078538239, blue: 0.2078139782, alpha: 1)
        containerView.cornerRadius = containerView.frame.height/2
        containerView.addTapGestureRecognizer { [weak self] in
            guard let self = self else {return}
            self.unlockAction?()
        }
    }

}
