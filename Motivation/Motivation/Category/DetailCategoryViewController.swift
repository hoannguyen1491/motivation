//
//  DetailCategoryViewController.swift
//  Motivation
//
//  Created by hoanglinh on 07/03/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class DetailCategoryViewController: UIViewController {

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var navBar: UINavigationBar!
    
    var idCate = 1
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var data: [VideoObject] = [VideoObject]()
    
    weak var delegate: CategoryViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
    }
    
    private func setupView() {
        self.view.backgroundColor = .black
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
        navBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        contentLabel.text = "A simple UI component/animation for double menu distribution in application."
        contentLabel.textColor = .black
        
        collectionView.backgroundColor = .clear
        collectionView.register(UINib(nibName: "CategoryHorizontalCell", bundle: nil), forCellWithReuseIdentifier: "CategoryHorizontalCell")
        collectionView.register(UINib(nibName: "UnlockAllCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UnlockAllCollectionViewCell")
        collectionView.isScrollEnabled = false
        
        requestGetDataListCategory()
    }
    
    @IBAction func leftButtonBarDidTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: -Request API
    private func requestGetDataListCategory() {
        APIRequest.share.getListVideoOfCategory(idCate: idCate) { [weak self] (data, error) in
            guard let `self` = self, let videos = data else { return }
            self.data = videos
            self.collectionView.reloadData()
        }
    }
    
    deinit {
        print("deinit")
    }
}

//MARK: - CollectionView
extension DetailCategoryViewController:  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let row = indexPath.row
        
        if row == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UnlockAllCollectionViewCell", for: indexPath) as! UnlockAllCollectionViewCell
            cell.setDataToCell()
            cell.unlockAction = { [weak self] in
                guard let self = self else {return}
                print("Unlock Click")
            }
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryHorizontalCell", for: indexPath) as! CategoryHorizontalCell
        cell.setDataToCell(data: data, type: 1, typeCate: .normal)
        cell.bgImageView.image = nil
        cell.titleCellLabel.text = ""
        cell.titleCellLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        cell.cstHeightTitleCell.constant = 0
        cell.heightCell = 350
        cell.widthCell = UIScreen.main.bounds.width*2/3
        cell.didClickCate = { [weak self] (id, videoID) in
            guard let `self` = self else {return}
            self.delegate?.categoryViewController(self, categoryIdClick: id, videoID: videoID)
            self.customNavigationBar_BackClick(self.navBar)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let row = indexPath.row
        if row == 1 {
            return CGSize(width: collectionView.frame.width , height: 150)
        }
        return CGSize(width: collectionView.frame.width , height: 350)
    }
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}
extension DetailCategoryViewController: CustomNavigationBarDelegate {
    func customNavigationBar_BackClick(_ view: UIView) {
        self.navigationController?.popViewController(animated: true)
    }
}
