//
//  SettingViewController.swift
//  Motivation
//
//  Created by TopTomsk on 08/03/2021.
//

import UIKit

class SettingViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var tableView: UITableView!
    
    var arrayTitle = [L10n.Text.reminder, L10n.Text.video, L10n.Text.widget]
    var arrayImage = [Asset.icReminder.name, Asset.icVideo.name, Asset.icWidget.name]
    var didAnimated = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: L10n.Text.defaultTableviewCell)
        tableView.separatorStyle = .none
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !didAnimated {
            self.view.alpha = 0
            self.view.cornerRadius = 16
            view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            UIView.animate(withDuration: 0.1, animations: { [weak self] in
                self?.view.cornerRadius = 0
                self?.view.transform = CGAffineTransform.identity
                self?.view.alpha = 1
            })
            didAnimated = true
        }
        
    }

    @IBAction func backBtn(_ sender: Any) {
        self.view.cornerRadius = 16
        UIView.animate(withDuration: 0.1) {
            self.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            self.view.alpha = 0
        } completion: { (_) in
            self.navigationController?.popViewController(animated: false)
        }

    }

}

//MARK: - tableview Delegate and Datasource
extension SettingViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTitle.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: L10n.Text.defaultTableviewCell)!
        cell.imageView?.image = UIImage(named: arrayImage[indexPath.row])?.resizableImage(withCapInsets: .init(top: 10, left: 10, bottom: 10, right: 10), resizingMode: .stretch)
        cell.textLabel?.text = arrayTitle[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        cell.backgroundColor = .clear
        cell.selectionStyle = .blue
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let container = UIView()
        container.backgroundColor = .clear
        let btnUnlock = UnlockButton(type: .system)
        btnUnlock.tintColor = .orange
        btnUnlock.setTitleColor(.lightGray, for: .highlighted)
        container.addSubview(btnUnlock)
        btnUnlock.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            btnUnlock.centerYAnchor.constraint(equalTo: container.centerYAnchor, constant: 20),
            btnUnlock.centerXAnchor.constraint(equalTo: container.centerXAnchor),
            btnUnlock.heightAnchor.constraint(equalToConstant: 50),
            btnUnlock.widthAnchor.constraint(equalToConstant: 180)
        ])
        btnUnlock.cornerRadius = 25
        return container
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    //MARK: Action click
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            let reminder = ReminderSettingViewController()
            self.navigationController?.pushViewController(reminder, animated: true)
        case 1:
            let videoVC = VideoSettingViewController()
            self.navigationController?.pushViewController(videoVC, animated: true)
        case 2:
            if #available(iOS 14.0, *) {
                let widget = WidgetSettingViewController()
                self.navigationController?.pushViewController(widget, animated: true)
            } else {
                // Fallback on earlier versions
            }
            
        default:
            break
        }
        
    }
    
}
