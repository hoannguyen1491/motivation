//
//  WidgetTableViewCell.swift
//  Motivation
//
//  Created by TopTomsk on 09/03/2021.
//

import UIKit

enum WidgetType {
    case normal
    case square
}

class WidgetTableViewCell: UITableViewCell {

    @IBOutlet weak var imgTheme: UIImageView!
    @IBOutlet weak var lbThemeName: UILabel!
    @IBOutlet weak var heightPreview: NSLayoutConstraint!
    @IBOutlet weak var viewType: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgTheme.dimImage()
    }
    
    func setupCell(type: WidgetType = .normal) {
        heightPreview.constant = SCREEN_WIDTH/4
        if type == .normal {
            viewType.snp.makeConstraints { (make) in
                make.width.equalTo(SCREEN_WIDTH/2)
            }
            lbThemeName.text = "Standard"
        } else {
            viewType.snp.makeConstraints { (make) in
                make.width.equalTo(viewType.snp.height)
            }
            lbThemeName.text = "Square"
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
