//
//  DetailWidgetTableViewCell.swift
//  Motivation
//
//  Created by TopTomsk on 10/03/2021.
//

import UIKit

class DetailWidgetTableViewCell: UITableViewCell {

    @IBOutlet weak var imgTheme1: UIImageView!
    @IBOutlet weak var imgTheme2: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgTheme2.dimImage()
        imgTheme1.dimImage()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
