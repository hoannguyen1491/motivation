//
//  ThemeCollectionViewCell.swift
//  Motivation
//
//  Created by TopTomsk on 10/03/2021.
//

import UIKit

class ThemeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageView.dimImage()
        labelText.text = "ABCD"
    }

}
