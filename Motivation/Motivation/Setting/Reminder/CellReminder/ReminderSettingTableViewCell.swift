//
//  ReminderSettingTableViewCell.swift
//  Motivation
//
//  Created by TopTomsk on 09/03/2021.
//

import UIKit

class ReminderSettingTableViewCell: UITableViewCell {

    @IBOutlet weak var numberOfReminder: UILabel!
    
    var number = 5 {
        didSet {
            Constant.share.getUserDefault().set(number, forKey: "numberOfPush")
            self.numberOfReminder.text = "\((number*2).description)x"
            Constant.share.setupLocalPush()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if let numberP = Constant.share.getUserDefault().object(forKey: "numberOfPush") as? Int {
            number = numberP
        }
        
        numberOfReminder.addTapGestureRecognizer { [weak self] in
            guard let self = self else {return}
            SystemSound.pressClick.play()
            
            //New value number reminder per day
            
                // Check transition animated (decrease -> from bottom, increase -> fromtop)
            let transition = CATransitionSubtype.fromTop
            self.numberOfReminder.pushTransition(0.3, from: transition)
                
            if self.number == 10 {
                self.number = 3
            } else {
                self.number += 1
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
