//
//  ReminderSettingViewController.swift
//  Motivation
//
//  Created by TopTomsk on 09/03/2021.
//

import UIKit

class ReminderSettingViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "ReminderSettingTableViewCell", bundle: nil), forCellReuseIdentifier: "ReminderSettingTableViewCell")
        tableView.separatorStyle = .none
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
        
    }

    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension ReminderSettingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReminderSettingTableViewCell") as! ReminderSettingTableViewCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let container = UIView()
        container.backgroundColor = .clear
        let btnAddWidget = UIButton(type: .system)
        container.addSubview(btnAddWidget)
        btnAddWidget.translatesAutoresizingMaskIntoConstraints = false
        btnAddWidget.contentEdgeInsets = .init(top: 0, left: 20, bottom: 0, right: 20)
        NSLayoutConstraint.activate([
            btnAddWidget.centerYAnchor.constraint(equalTo: container.centerYAnchor, constant: 20),
            btnAddWidget.centerXAnchor.constraint(equalTo: container.centerXAnchor),
            btnAddWidget.heightAnchor.constraint(equalToConstant: 50),
        ])
        btnAddWidget.backgroundColor = .black
        btnAddWidget.titleLabel?.font = .boldSystemFont(ofSize: 16)
        btnAddWidget.setTitleColor(.white, for: .normal)
        btnAddWidget.setTitle("Unlock more reminder", for: .normal)
        btnAddWidget.cornerRadius = 25
        return container
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
}
