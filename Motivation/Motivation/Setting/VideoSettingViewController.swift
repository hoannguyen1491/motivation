//
//  VideoSettingViewController.swift
//  Motivation
//
//  Created by TopTomsk on 09/03/2021.
//

import UIKit

class VideoSettingViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var switchAutoNext: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
        
        switchAutoNext.isUserInteractionEnabled = true
        switchAutoNext.addTarget(self, action: #selector(switchDidChanged(sender:)), for: .valueChanged)
        if let val = Constant.share.getUserDefault().object(forKey: "autoNext") as? Bool {
            switchAutoNext.isOn = val
        } else {
            switchAutoNext.isOn = false
        }
    }

    @objc func switchDidChanged(sender: UISwitch) {
        Constant.share.getUserDefault().set(sender.isOn, forKey: "autoNext")
    }
    
    
    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
