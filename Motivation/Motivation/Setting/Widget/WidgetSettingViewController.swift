//
//  WidgetSettingViewController.swift
//  Motivation
//
//  Created by TopTomsk on 09/03/2021.
//

import UIKit
import GoogleMobileAds
import SwiftUI
import WidgetKit

extension UserDefaults {
  static let group = UserDefaults(suiteName: "group.com.hoan.Motivations.gr")!
}

@available(iOS 14.0, *)
class WidgetSettingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navBar: UINavigationBar!
    
    @AppStorage("widgets", store: UserDefaults(suiteName: "group.com.hoan.Motivations.gr"))
    var widgetData: Data = Data()
    
    var rewardAds = GADRewardedAd()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "WidgetTableViewCell", bundle: nil), forCellReuseIdentifier: "WidgetTableViewCell")
        tableView.separatorStyle = .none
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
        
        Constant.share.getUserDefault().setValue("123456789", forKey: "test")
    }

    @IBAction func backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
}
@available(iOS 14.0, *)
extension WidgetSettingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WidgetTableViewCell") as! WidgetTableViewCell
        cell.setupCell(type: indexPath.row % 2 == 0 ? .normal : .square)
        cell.selectionStyle = .none
        cell.addTapGestureRecognizer { [weak self] in
            guard let `self` = self else {return}
            let detail = DetailWidgetViewController()
            self.navigationController?.pushViewController(detail, animated: true)
            
        }
        
        if let idTheme = Constant.share.getUserDefault().object(forKey: "idTheme") as? Int {
            cell.imgTheme.image = UIImage(data: Constant.share.getThemes()[idTheme].theme)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let container = UIView()
        container.backgroundColor = .clear
        let btnAddWidget = UIButton(type: .system)
        container.addSubview(btnAddWidget)
        btnAddWidget.translatesAutoresizingMaskIntoConstraints = false
        btnAddWidget.contentEdgeInsets = .init(top: 0, left: 20, bottom: 0, right: 20)
        NSLayoutConstraint.activate([
            btnAddWidget.centerYAnchor.constraint(equalTo: container.centerYAnchor, constant: 20),
            btnAddWidget.centerXAnchor.constraint(equalTo: container.centerXAnchor),
            btnAddWidget.heightAnchor.constraint(equalToConstant: 50),
        ])
        btnAddWidget.backgroundColor = .black
        btnAddWidget.titleLabel?.font = .boldSystemFont(ofSize: 16)
        btnAddWidget.setTitleColor(.white, for: .normal)
        btnAddWidget.setTitle("Add custom widget", for: .normal)
        btnAddWidget.addTarget(self, action: #selector(addViewAds), for: .touchUpInside)
        btnAddWidget.cornerRadius = 25
        return container
    }
    
    @objc func addViewAds() {
        let viewAd = ViewAdsAndIAP()
        viewAd.delegate = self
        view.addSubViewWithFullBlur(subV: viewAd)
        viewAd.snp.makeConstraints { (make) in
            make.centerY.centerX.equalToSuperview()
            make.width.equalTo(280)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
}
@available(iOS 14.0, *)
extension WidgetSettingViewController: ViewAdsAndIAPDelegate {
    func viewAdsAndIAP_adsClick(_ view: UIView) {
        let request = GADRequest()
        GADRewardedAd.load(withAdUnitID: "ca-app-pub-3940256099942544/1712485313", request: request) { (reward, err) in
            guard let `reward` = reward, err == nil else {
                print(err)
                return
            }
            self.rewardAds = reward
            self.rewardAds.fullScreenContentDelegate = self
            self.rewardAds.present(fromRootViewController: self) {
                print("did Earned")
            }
        }
    }
    
    func viewAdsAndIAP_iapClick(_ view: UIView) {
        
    }
}

@available(iOS 14.0, *)
extension WidgetSettingViewController: GADFullScreenContentDelegate {
    
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("enter fullscreen")
    }
    
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
        print("failed enter fullscreen")
    }
    
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("dismiss fullscreen")
    }
}
