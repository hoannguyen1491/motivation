//
//  DetailWidgetViewController.swift
//  Motivation
//
//  Created by TopTomsk on 10/03/2021.
//

import UIKit
@available(iOS 14.0, *)
class DetailWidgetViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewSetting: UITableView!
    
    @IBOutlet weak var tableViewQuote: UITableView!
    @IBOutlet weak var settingView: UIView!
    @IBOutlet weak var cstHeightTableViewSetting: NSLayoutConstraint!
    @IBOutlet weak var cstHeightTableViewQuote: NSLayoutConstraint!
    
    var arrayTitleCate: [String] = []
    var arrayData: [String] = ["4 times on day", "6 times on day", "8 times on day", "12 times on day", "24 times on day", "Close"]
    var indexCheckMarkQuote: Int = 0
    var indexCheckMarkTimes: Int = 0
    var dataCategory = [CategoryObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "DetailWidgetTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailWidgetTableViewCell")
        self.tableView.tableFooterView = UIView()
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
        settingView.isHidden = true
        tableViewSetting.isHidden = true
        tableViewQuote.isHidden = true
        
        if let timesSelect = UserDefaults.standard.object(forKey: "SelectTimes") as? Int {
            self.indexCheckMarkTimes = timesSelect
        }
        
        if let categorySelect = UserDefaults.standard.object(forKey: "SelectCategory") as? Int {
            self.indexCheckMarkQuote = categorySelect
        }
        
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = .darkGray
        
        getDataCategorys()
    }

    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    //MARK: -RequestAPI
    private func getDataCategorys() {
        APIRequest.share.getCategoryVideos { [weak self] (data, error) in
            guard let `self` = self else { return }
            
            guard error == nil else {
                self.arrayTitleCate.append("Close")
                return
            }
            
            do {
                let categorys = try JSONDecoder().decode([CategoryObject].self, from: data as! Data)
                //print(categorys)
                self.dataCategory = categorys
                for i in 0..<categorys.count {
                    self.arrayTitleCate.append(categorys[i].category_name)
                }
                self.arrayTitleCate.append("Close")
            } catch {
                print(error.localizedDescription)
            }

        }
    }
}

@available(iOS 14.0, *)
extension DetailWidgetViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewSetting {
            return arrayData.count
        } else if tableView == tableViewQuote {
            return arrayTitleCate.count
        }
        
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case tableViewSetting:
            let cell = UITableViewCell.init(style: .value1, reuseIdentifier: "cell1")
            cell.textLabel?.text = arrayData[indexPath.row]
            cell.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
            cell.accessoryType = .none
            
            if indexPath.row == indexCheckMarkTimes {
                cell.accessoryType = .checkmark
            }
            return cell
        case tableViewQuote:
            let cell = UITableViewCell.init(style: .value1, reuseIdentifier: "cell1")
            cell.textLabel?.text = arrayTitleCate[indexPath.row]
            cell.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
            cell.accessoryType = .none
            
            if indexPath.row == indexCheckMarkQuote {
                cell.accessoryType = .checkmark
            }
            return cell
        default:
            let cell = UITableViewCell.init(style: .value1, reuseIdentifier: "cell")
            cell.selectionStyle = .none
            cell.backgroundColor = .clear
            cell.tintColor = .white
            cell.textLabel?.textColor = .white
            cell.detailTextLabel?.textColor = .lightGray
            
            let accessoryImage = UIImageView(image: Asset.rightChevron.image.imageResized(to: .init(width: 10, height: 10)))
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Standard"
                cell.textLabel?.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
                cell.accessoryType = .none
            case 1:
                cell.textLabel?.text = "Change theme"
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
                cell.accessoryView = accessoryImage
            case 3:
                cell.textLabel?.text = "Refresh frequency"
                cell.detailTextLabel?.text = "Often (8-12 times/day)"
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
                cell.accessoryView = accessoryImage
            case 4:
                cell.textLabel?.text = "Type of quote"
                cell.detailTextLabel?.text = "General"
                cell.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
                cell.accessoryView = accessoryImage
            default:
                let cellDetail = tableView.dequeueReusableCell(withIdentifier: "DetailWidgetTableViewCell") as! DetailWidgetTableViewCell
                cellDetail.selectionStyle = .none
                cellDetail.backgroundColor = .clear
                if let idTheme = Constant.share.getUserDefault().object(forKey: "idTheme") as? Int {
                    let img = UIImage(data: Constant.share.getThemes()[idTheme].theme)
                    cellDetail.imgTheme1.image = img
                    cellDetail.imgTheme2.image = img
                }
                return cellDetail
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableViewSetting {
            self.cstHeightTableViewSetting.constant = CGFloat(50 * self.arrayData.count)
            if self.cstHeightTableViewSetting.constant >= SCREEN_HEIGHT {
                self.cstHeightTableViewSetting.constant = SCREEN_HEIGHT - 100
            }
            return 50
        } else if tableView == tableViewQuote {
            self.cstHeightTableViewQuote.constant = CGFloat(50 * self.arrayTitleCate.count)
            if self.cstHeightTableViewQuote.constant >= SCREEN_HEIGHT {
                self.cstHeightTableViewQuote.constant = SCREEN_HEIGHT - 100
            }
            return 50
        } else {
            if indexPath.row == 2 {
                return SCREEN_WIDTH - 32
            }
            return 68
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case tableViewSetting:
            if let cell = tableView.cellForRow(at: indexPath) {
                if indexPath.row != self.arrayData.count - 1 {
                    cell.accessoryType = .checkmark
                    indexCheckMarkTimes = indexPath.row
                    UserDefaults.standard.setValue(indexPath.row, forKey: "SelectTimes")
                }
            }
            
            self.tableViewSetting.isHidden = true
            self.settingView.isHidden = true
            break
        case tableViewQuote:
            if let cell = tableView.cellForRow(at: indexPath) {
                if indexPath.row != self.arrayTitleCate.count - 1 {
                    cell.accessoryType = .checkmark
                    indexCheckMarkQuote = indexPath.row
                    UserDefaults.standard.setValue(indexPath.row, forKey: "SelectCategory")
                }
            }
            
            self.tableViewQuote.isHidden = true
            self.settingView.isHidden = true
            break
        default:
            if indexPath.row == 1 || indexPath.row == 2 {
                let themes = CollectionThemeViewController()
                var arrayQuote: [String] = []
                
                if dataCategory.count > 0 {
                    for i in 0..<self.dataCategory[indexCheckMarkQuote].video_info.count {
                        arrayQuote.append(self.dataCategory[indexCheckMarkQuote].video_info[i].quotes)
                    }
                    themes.arrayQuote = arrayQuote
                } else {
                    themes.arrayQuote = ["4 times on day", "6 times on day", "8 times on day", "12 times on day", "24 times on day"]
                }
                
                themes.times = self.indexCheckMarkTimes
                self.navigationController?.pushViewController(themes, animated: true)
            } else if indexPath.row == 3 {
                //Select show quote times/day
                self.settingView.isHidden = false
                self.tableViewSetting.isHidden = false
                self.tableViewQuote.isHidden = true
                self.tableViewSetting.reloadData()
            } else if indexPath.row == 4 {
                //Select type quote
                self.settingView.isHidden = false
                self.tableViewSetting.isHidden = true
                self.tableViewQuote.isHidden = false
                self.tableViewQuote.reloadData()
            }
            break
        }
        //
    }
}
