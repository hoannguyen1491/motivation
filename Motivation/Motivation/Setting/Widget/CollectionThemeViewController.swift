//
//  CollectionThemeViewController.swift
//  Motivation
//
//  Created by TopTomsk on 10/03/2021.
//

import UIKit
import WidgetKit
@available(iOS 14.0, *)
class CollectionThemeViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var currentCell: ThemeCollectionViewCell?
    var arrayQuote: [String] = []
//    var widgetObjs: [WidgetObjects] = []
    var times: Int = 1
    
    var timer : Timer?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionView.register(UINib(nibName: "ThemeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ThemeCollectionViewCell")
        collectionView.register(UINib(nibName: "TextCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TextCollectionViewCell")
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(animateFrame), userInfo: nil, repeats: true)
    }
    
    @objc func animateFrame() {
        // Do something
        WidgetCenter.shared.reloadAllTimelines()
    }

    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

@available(iOS 14.0, *)
extension CollectionThemeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Constant.share.getThemes().count + 2
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextCollectionViewCell", for: indexPath) as! TextCollectionViewCell
            cell.labelText.text = "Themes"
            cell.labelText.font = UIFont.systemFont(ofSize: 26, weight: .semibold)
            cell.labelText.textColor = .white
            return cell
        } else if indexPath.row == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextCollectionViewCell", for: indexPath) as! TextCollectionViewCell
            cell.labelText.text = "Edit current theme".uppercased()
            cell.labelText.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
            cell.labelText.textColor = .white
            cell.labelText.backgroundColor = UIColor(rgb: 0x02D19F)
            cell.labelText.cornerRadius = 22
            cell.labelText.textAlignment = .center
            cell.addTapGestureRecognizer { [weak self] in
                guard let self = self else {return}
                let edit = EditThemeViewController()
                if let idx = Constant.share.getUserDefault().object(forKey: "idTheme") as? Int {
                    edit.index = idx
                }
                edit.didSave = {
                    collectionView.reloadData()
                }
                self.navigationController?.pushViewController(edit, animated: true)
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThemeCollectionViewCell", for: indexPath) as! ThemeCollectionViewCell
            cell.imgCheck.isHidden = true
            cell.imageView.image = UIImage(data: Constant.share.getThemes()[indexPath.row - 2].theme)
            if let idTheme = Constant.share.getUserDefault().object(forKey: "idTheme") as? Int {
                if idTheme == indexPath.row - 2 {
                    cell.imgCheck.isHidden = false
                    currentCell = cell
                }
            } else {
                if indexPath.row == 2 {
                    cell.imgCheck.isHidden = false
                    currentCell = cell
                }
            }
            cell.contentView.cornerRadius = 6
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let _ = collectionView.cellForItem(at: indexPath) as? ThemeCollectionViewCell {
            if let userDefaults = UserDefaults(suiteName: "group.com.hoan.Motivations.gr") {
                userDefaults.set(indexPath.row - 2, forKey: "idTheme")
                if Constant.share.getThemes().indices.contains(indexPath.row - 2) {
                    let data = Constant.share.getThemes()[indexPath.row - 2].theme
                    Constant.share.getUserDefault().set(data, forKey: "backgroundWidget")
                    //widgetObjs = [WidgetObjects(title: "", icon: "", content: "", bgImage: data)]
                    
                    Constant.share.getUserDefault().set(arrayQuote.randomElement(), forKey: "quoteWidget")
                    
//                    guard let widgetData = try? JSONEncoder().encode(widgetObjs) else {
//                        return
//                    }
//                    userDefaults.set(widgetData, forKey: "dataImageTheme")
                    userDefaults.synchronize()
                    
                    WidgetCenter.shared.reloadAllTimelines()
                }
            }
            
            collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row <= 1 {
            return .init(width: SCREEN_WIDTH - 32, height: 44)
        } else {
            let w = Int((SCREEN_WIDTH - 48)/3)
            return .init(width: w, height: w)
        }
    }
}
