//
//  EditThemeViewController.swift
//  Motivation
//
//  Created by TopTomsk on 10/03/2021.
//

import UIKit
import RealmSwift
import WidgetKit

@available(iOS 14.0, *)
class EditThemeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIColorPickerViewControllerDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var imgTheme: UIImageView!
    
    @IBOutlet weak var btnTextColor: UIButton!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var btnBackground: UIButton!
    @IBOutlet weak var btnTextShadowColor: UIButton!
    @IBOutlet weak var btnTextFont: UIButton!
    @IBOutlet weak var btnTextAlign: UIButton!
    @IBOutlet weak var btnCapital: UIButton!
    @IBOutlet weak var btnTextSize: UIButton!
    @IBOutlet weak var lbText: UILabel!
    
    var currentColor: String = "0xFFFFFF"
    var currentAlign: Int = AlignWidget.left.rawValue
    var currentCapital: Bool = false
    var currentFontSize: CGFloat = 18
    var currentFontName: String = "AppleSDGothicNeo-Regular"
    
    enum AlignWidget: Int {
        case left = 0
        case center = 1
        case right = 2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
        let realm = try! Realm()
        if let objs = realm.objects(ThemeObject.self).filter("index == \(self.index)").first {
            self.imgTheme.image = UIImage(data: objs.theme)
        }
        btnImage.imageView?.contentMode = .scaleAspectFit
        
        btnTextColor.borderColor = .lightGray
        btnTextColor.borderWidth = 0.5
        
        if let font = Constant.share.getUserDefault().object(forKey: "WidgetFont") as? String {
            currentFontName = font
            if let size = Constant.share.getUserDefault().object(forKey: "fontsize") as? CGFloat {
                currentFontSize = size
                lbText.font = UIFont(name: font, size: size)
                btnTextSize.titleLabel?.font = btnTextSize.titleLabel?.font.withSize(size - 2)
            } else {
                lbText.font = UIFont(name: font, size: 18)
                btnTextSize.titleLabel?.font = btnTextSize.titleLabel?.font.withSize(18 - 2)
            }
        } else {
            lbText.font = UIFont(name: currentFontName, size: 18)
            btnTextSize.titleLabel?.font = btnTextSize.titleLabel?.font.withSize(18 - 2)
        }
        if let capital = Constant.share.getUserDefault().object(forKey: "capital") as? Bool {
            currentCapital = capital
            lbText.text = capital ? lbText.text?.uppercased() : lbText.text?.lowercased()
            btnCapital.setTitle(capital ? "AA" : "aa", for: .normal)
        } else {
            btnCapital.setTitle("aa".capitalizingFirstLetter(), for: .normal)
            lbText.text = lbText.text?.lowercased().capitalizingFirstLetter()
            currentCapital = false
        }
        if let hex = Constant.share.getUserDefault().object(forKey: "textColor") as? String{
            if let colorInt = Int(hex, radix: 16) {
                currentColor = hex
                btnTextColor.backgroundColor = UIColor(rgb: colorInt)
                lbText.textColor = UIColor(rgb: colorInt)
            }
        } else {
            lbText.textColor = .white
            btnTextColor.backgroundColor = .white
        }
        if let align = Constant.share.getUserDefault().object(forKey: "align") as? Int {
            currentAlign = align
            switch align {
            case AlignWidget.left.rawValue:
                lbText.textAlignment = .left
                btnTextAlign.setImage(Asset.leftAlignment.image, for: .normal)
            case AlignWidget.center.rawValue:
                lbText.textAlignment = .center
                btnTextAlign.setImage(Asset.alignment.image, for: .normal)
            default:
                lbText.textAlignment = .right
                btnTextAlign.setImage(Asset.rightAlignment.image, for: .normal)
            }
        } else {
            lbText.textAlignment = .left
            currentAlign = AlignWidget.left.rawValue
            btnTextAlign.setImage(Asset.leftAlignment.image, for: .normal)
        }
    }
    
    @IBAction func cancelClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    var index = 0
    var didSave: (()->())?
    
    @IBAction func saveClick(_ sender: Any) {
        Constant.share.getUserDefault().set(self.currentColor, forKey: "textColor")
        Constant.share.getUserDefault().set(currentAlign, forKey: "align")
        Constant.share.getUserDefault().set(currentCapital, forKey: "capital")
        Constant.share.getUserDefault().set(currentFontSize, forKey: "fontsize")
        Constant.share.getUserDefault().set(currentFontName, forKey: "WidgetFont")
        
        if let img = imgTheme.image {
            let realm = try! Realm()
            let objs = realm.objects(ThemeObject.self).filter("index == \(self.index)")
            if let obj = objs.first, let data = img.jpegData(compressionQuality: 100) {
                try! realm.write {
                    obj.theme = data
                    obj.index = self.index
                }
                if let userDefaults = UserDefaults(suiteName: "group.com.hoan.Motivations.gr") {
                    userDefaults.set(data, forKey: "backgroundWidget")
                    userDefaults.synchronize()
                }
            }
            didSave?()
        }
        WidgetCenter.shared.reloadAllTimelines()
        self.navigationController?.popViewController(animated: true)
    }
    
    var imagePicker = UIImagePickerController()
    
    @IBAction func editBtnClicked(_ sender: UIButton) {
        SystemSound.pressClick.play()
        switch sender.tag {
        case 0:
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")

                imagePicker.delegate = self
                imagePicker.sourceType = .savedPhotosAlbum
                imagePicker.allowsEditing = false

                present(imagePicker, animated: true, completion: nil)
            }
        case 1,2,3:
            let colorPicker = UIColorPickerViewController()
            colorPicker.delegate = self
            colorPicker.view.tag = sender.tag
            present(colorPicker, animated: true, completion: nil)
        case 4:
            let configuration = UIFontPickerViewController.Configuration()
            configuration.includeFaces = true
            configuration.displayUsingSystemFont = true
            configuration.filteredTraits = [.classModernSerifs]
            let vc = UIFontPickerViewController(configuration: configuration)
            vc.delegate = self
            present(vc, animated: true)
        case 5:
            var alignment: Int = AlignWidget.left.rawValue
            if let align = Constant.share.getUserDefault().object(forKey: "align") as? Int {
                alignment = align
            } else {
                alignment = currentAlign
            }
            switch alignment {
            case AlignWidget.left.rawValue:
                lbText.textAlignment = .center
                currentAlign = AlignWidget.center.rawValue
                btnTextAlign.setImage(Asset.alignment.image, for: .normal)
            case AlignWidget.center.rawValue:
                lbText.textAlignment = .right
                currentAlign = AlignWidget.right.rawValue
                btnTextAlign.setImage(Asset.rightAlignment.image, for: .normal)
            default:
                lbText.textAlignment = .left
                currentAlign = AlignWidget.left.rawValue
                btnTextAlign.setImage(Asset.leftAlignment.image, for: .normal)
            }
        case 6:
            if let capital = Constant.share.getUserDefault().object(forKey: "capital") as? Bool {
                lbText.text = capital ? lbText.text?.lowercased().capitalizingFirstLetter() : lbText.text?.uppercased()
                btnCapital.setTitle(capital ? "aa".capitalizingFirstLetter() : "AA", for: .normal)
                currentCapital = !capital
            } else {
                btnCapital.setTitle(!currentCapital ? "aa".uppercased() : "aa".capitalizingFirstLetter(), for: .normal)
                lbText.text = !currentCapital ? lbText.text?.uppercased() : lbText.text?.lowercased().capitalizingFirstLetter()
                currentCapital = !currentCapital
            }
        case 7:
            if let size = Constant.share.getUserDefault().object(forKey: "fontsize") as? CGFloat {
                var fontsize: CGFloat = 18
                if size == 18 {
                    fontsize = 24
                } else if size == 24 {
                    fontsize = 14
                }
                lbText.font = lbText.font.withSize(fontsize)
                currentFontSize = fontsize
            } else {
                lbText.font = lbText.font.withSize(24)
                currentFontSize = 24
            }
            
        default:
            break
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imgTheme.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func colorPickerViewControllerDidSelectColor(_ viewController: UIColorPickerViewController) {
        switch viewController.view.tag {
        case 1:
            btnBackground.backgroundColor = viewController.selectedColor
            imgTheme.image = viewController.selectedColor.image()
        case 2:
            btnTextColor.backgroundColor = viewController.selectedColor
            lbText.textColor = viewController.selectedColor
            self.currentColor = hexStringFromColor(color: viewController.selectedColor)
        default:
            btnTextShadowColor.backgroundColor = viewController.selectedColor
        }
        
    }
    func hexStringFromColor(color: UIColor) -> String {
        let components = color.cgColor.components
        let r: CGFloat = components?[0] ?? 0.0
        let g: CGFloat = components?[1] ?? 0.0
        let b: CGFloat = components?[2] ?? 0.0

        let hexString = String.init(format: "%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
        print(hexString)
        return hexString
     }

    func colorPickerViewControllerDidFinish(_ viewController: UIColorPickerViewController) {
        // view.backgroundColor = viewController.selectedColor
    }
    
}
@available(iOS 14.0, *)
extension EditThemeViewController: UIFontPickerViewControllerDelegate {
    func fontPickerViewControllerDidPickFont(_ viewController: UIFontPickerViewController) {
        let font = UIFont(descriptor: viewController.selectedFontDescriptor!, size: 16)
        currentFontName = font.fontName
        lbText.font = UIFont(name: currentFontName, size: currentFontSize)
        WidgetCenter.shared.reloadAllTimelines()
    }
}
