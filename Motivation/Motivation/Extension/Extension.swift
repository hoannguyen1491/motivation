//
//  Extension.swift
//  Motivation
//
//  Created by TopTomsk on 05/03/2021.
//

import UIKit

let SCREEN_WIDTH = UIScreen.main.bounds.width
let SCREEN_HEIGHT = UIScreen.main.bounds.height

extension UIImage {
    func imageResized(to size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }
    }
}

extension UIImageView {
    
    //Dim image with blur layer
    func dimImage() {
        let coverLayer = CALayer()
        coverLayer.frame = self.bounds;
        coverLayer.backgroundColor = UIColor.black.cgColor
        coverLayer.opacity = 0.1
        self.layer.addSublayer(coverLayer)
    }
}
extension UIView {
    
    //animation multi View in sequence
    func animatedItems(collectionItemsView: [UIView], duration: Double = 0.5) {
        let totalTimeanimated = Double(collectionItemsView.count)*duration
        UIView.animateKeyframes(withDuration: totalTimeanimated,
                                delay: 0,
                                options: .beginFromCurrentState,
                                animations: {
            for (index, item) in collectionItemsView.enumerated() {
                UIView.addKeyframe(withRelativeStartTime: Double(index) / Double(collectionItemsView.count),
                                   relativeDuration: 1 / Double(collectionItemsView.count),
                                   animations: {
                    item.alpha = 1
                })
            }
        }, completion: nil)
    }
    
    func fadeIn(_ duration: TimeInterval? = 0.2, onCompletion: (() -> Void)? = nil) {
        self.alpha = 0
        self.isHidden = false
        UIView.animate(withDuration: duration!,
                       animations: { self.alpha = 1 },
                       completion: { (value: Bool) in
                          if let complete = onCompletion { complete() }
                       }
        )
    }

    func fadeOut(_ duration: TimeInterval? = 0.2, onCompletion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration!,
                       animations: { self.alpha = 0 },
                       completion: { (value: Bool) in
                           self.isHidden = true
                           if let complete = onCompletion { complete() }
                       }
        )
    }
    
    func addSubviewWithAnimation(subView: UIView) {
        let transition = CATransition()
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.layer.add(transition, forKey: nil)
        self.addSubview(subView)
    }
    
    @IBInspectable var borderColor: UIColor {
        set {
            layer.borderColor = newValue.cgColor
        }
        get {
            guard let color = layer.borderColor else {
                return .clear
            }
            return UIColor(cgColor: color)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if self.shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    func addShadow(shadowColor: UIColor = .lightGray,
                   shadowOffset: CGSize = CGSize(width: 0, height: 0),
                   shadowOpacity: Float = 0.5,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
}
let kBlurViewTag = 500
extension UIView {
    
    func pushTransition(_ duration:CFTimeInterval, from: CATransitionSubtype) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
                                                            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.push
        animation.subtype = from
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.push.rawValue)
    }
    
    func addSubViewWithFullBlur(subV: UIView) {
        let blur = UIView()
        blur.backgroundColor = .black
        blur.alpha = 0.4
        addSubview(blur)
        blur.tag = kBlurViewTag
//        blur.snp.makeConstraints { (make) in
//            make.edges.equalToSuperview()
//        }
        blur.addTapGestureRecognizer {
            blur.removeFromSuperview()
            subV.removeFromSuperview()
        }
        addSubview(subV)
    }
    
    func removeViewFromSuperViewWithblur() {
        if let spV = self.superview {
            let aa = spV.subviews.filter({ (subv) -> Bool in
                return subv.tag == kBlurViewTag
            })
            if aa.count == 1 {
                aa[0].removeFromSuperview()
            }
        }
        self.removeFromSuperview()
    }
    
    fileprivate struct AssociatedObjectKeys {
        static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer"
    }
    fileprivate typealias Action = (() -> Void)?
    fileprivate var tapGestureRecognizerAction: Action? {
        set {
            if let newValue = newValue {
                // Computed properties get stored as associated objects
                objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? Action
            return tapGestureRecognizerActionInstance
        }
    }
    
    func addGestureRecognizers(_ gestureRecognizers: [UIGestureRecognizer]) {
        for recognizer in gestureRecognizers {
            addGestureRecognizer(recognizer)
        }
    }
    
    @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let action = self.tapGestureRecognizerAction {
            action?()
        } else {
            print("no action")
        }
    }
    
    public func addTapGestureRecognizer(action: (() -> Void)?) {
        self.isUserInteractionEnabled = true
        self.tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
}

extension String {

    func chopPrefix(_ count: Int = 1) -> String {
        if count >= 0 && count <= self.count {
            let indexStartOfText = self.index(self.startIndex, offsetBy: count)
            return String(self[indexStartOfText...])
        }
        return ""
    }

    func chopSuffix(_ count: Int = 1) -> String {
        if count >= 0 && count <= self.count {
            let indexEndOfText = self.index(self.endIndex, offsetBy: -count)
            return String(self[..<indexEndOfText])
        }
        return ""
    }
    
    func convertToURL() -> URL? {
        return URL(string: self)
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func convertToRealURLString() -> String {
        let replaceSace = self.replacingOccurrences(of: " ", with: L10n.Url.spaceCharacter)
        return "\(L10n.Url.base)\(L10n.Url.videoUrl)\(replaceSace)"
    }
    
}
extension UIColor {
    convenience init(rgb: Int) {
        self.init(rgb: rgb, alpha: 1.0)
    }

    convenience init(rgb: Int, alpha: CGFloat) {
        let red = CGFloat(rgb >> 16) / 255.0
        let green = CGFloat(rgb >> 8 & 0xff) / 255.0
        let blue = CGFloat(rgb >> 0 & 0xff) / 255.0

        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}

enum IntroStep: Int {
    case kGetStart = 0
    case kReminder = 1
    case kCategory = 2
    case kWidget = 3
}

extension UIViewController {
    func setBackgroundImage(name: String, belowSubview: UIView) {
        let img = UIImageView(image: UIImage(named: name))
        img.contentMode = .scaleAspectFill
        img.frame = view.bounds
        view.insertSubview(img, belowSubview: belowSubview)
    }
}

class UnlockButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        self.backgroundColor = .black
        self.setTitle("Go Premium", for: .normal)
        self.setImage(UIImage(named: "ic_crown"), for: .normal)
        self.titleLabel?.font = .boldSystemFont(ofSize: 16)
        self.setTitleColor(.white, for: .normal)
        self.imageEdgeInsets = .init(top: 0, left: -8, bottom: 0, right: 0)
        self.titleEdgeInsets = .init(top: 0, left: 8, bottom: 0, right: 0)
    }
    
}

class ViewContainerSetting: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        self.backgroundColor = .clear
        let viewMain = UIView()
        let viewShadow = UIView()
        addSubview(viewShadow)
        addSubview(viewMain)
        
        viewMain.snp.makeConstraints { (make) in
            make.top.left.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-16)
        }

        viewShadow.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview()
        }
        viewMain.cornerRadius = 16
        viewShadow.cornerRadius = 16
        
        viewMain.backgroundColor = UIColor(rgb: 0x02D19F)
        viewShadow.backgroundColor = UIColor(rgb: 0x02D19F)
        viewShadow.alpha = 0.3
    }
}
public class UIPaddedLabel: UILabel {

    @IBInspectable var topInset: CGFloat = 0.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat = 0
    @IBInspectable var rightInset: CGFloat = 0

    public override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }

    public override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }

    public override func sizeToFit() {
        super.sizeThatFits(intrinsicContentSize)
    }
}

class GradientImageView: UIImageView {
    let gradient = UIImageView(image: UIImage(named: "gradient_img"))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        self.addSubview(gradient)
        gradient.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

import AudioToolbox
enum SystemSound: UInt32 {

    case pressClick    = 1123
    case pressDelete   = 1155
    case pressModifier = 1156
    case custom        = 1336

    func play() {
        AudioServicesPlaySystemSound(self.rawValue)
        let impactFeedBack = UIImpactFeedbackGenerator(style: .light)
        impactFeedBack.prepare()
        impactFeedBack.impactOccurred()
    }
}
class NavigationController: UINavigationController, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFullWidthBackGesture()
        self.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
       return topViewController?.preferredStatusBarStyle ?? .default
    }

    lazy var fullWidthBackGestureRecognizer = UIPanGestureRecognizer()

    private func setupFullWidthBackGesture() {
        guard
            let interactivePopGestureRecognizer = interactivePopGestureRecognizer,
            let targets = interactivePopGestureRecognizer.value(forKey: "targets")
        else {
            return
        }

        fullWidthBackGestureRecognizer.setValue(targets, forKey: "targets")
        fullWidthBackGestureRecognizer.delegate = self
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction))
        swipe.direction = .left
        view.addGestureRecognizer(swipe)
        fullWidthBackGestureRecognizer.require(toFail: swipe)
        view.addGestureRecognizer(fullWidthBackGestureRecognizer)
    }
    @objc func swipeAction() {}
}

extension NavigationController {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let isSystemSwipeToBackEnabled = interactivePopGestureRecognizer?.isEnabled == true
        let isThereStackedViewControllers = viewControllers.count > 1
        return isSystemSwipeToBackEnabled && isThereStackedViewControllers
    }
}
class VerticalTopAlignLabel: UILabel {

    override func drawText(in rect:CGRect) {
        guard let labelText = text else {  return super.drawText(in: rect) }

        let attributedText = NSAttributedString(string: labelText, attributes: [NSAttributedString.Key.font: font!])
        var newRect = rect
        newRect.size.height = attributedText.boundingRect(with: rect.size, options: .usesLineFragmentOrigin, context: nil).size.height

        if numberOfLines != 0 {
            newRect.size.height = min(newRect.size.height, CGFloat(numberOfLines) * font.lineHeight)
        }

        super.drawText(in: newRect)
    }

}
extension UICollectionView {
    func reloadData(_ completion: (() -> Void)? = nil) {
        reloadData()
        guard let completion = completion else { return }
        layoutIfNeeded()
        completion()
    }
}
extension UIColor {
    func image(_ size: CGSize = CGSize(width: SCREEN_WIDTH, height: SCREEN_HEIGHT)) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
}

extension Array where Element: Equatable {
    mutating func move(_ item: Element, to newIndex: Index) {
        if let index = firstIndex(of: item) {
            move(at: index, to: newIndex)
        }
    }

    mutating func bringToFront(item: Element) {
        move(item, to: 0)
    }

    mutating func sendToBack(item: Element) {
        move(item, to: endIndex-1)
    }
}

extension Array {
    mutating func move(at index: Index, to newIndex: Index) {
        insert(remove(at: index), at: newIndex)
    }
}
