//
//  CustomNavigationBar.swift
//  Tin
//
//  Created by nguyen hoan on 3/11/20.
//  Copyright © 2020 vietnb. All rights reserved.
//

import UIKit

protocol CustomNavigationBarDelegate: class {
    func customNavigationBar_BackClick(_ view: UIView)
}

class CustomNavigationBar: UIView {
    var title = UILabel()
    var btnBack = UIButton()
    weak var delegate: CustomNavigationBarDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        title.font = UIFont.systemFont(ofSize: IS_IPHONE ? 18 : 22, weight: .semibold)
        title.textColor = .black
        title.textAlignment = .center
        btnBack.setImage(UIImage(named: "ic_back_white"), for: .normal)
        
        addSubview(title)
        addSubview(btnBack)
        title.translatesAutoresizingMaskIntoConstraints = false
        btnBack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            //title.centerXAnchor.constraint(equalTo: centerXAnchor),
            title.leftAnchor.constraint(equalTo: leftAnchor, constant: 40),
            title.rightAnchor.constraint(equalTo: rightAnchor, constant: -40),
            title.centerYAnchor.constraint(equalTo: centerYAnchor),
            title.topAnchor.constraint(equalTo: topAnchor),
            
            btnBack.topAnchor.constraint(equalTo: topAnchor),
            btnBack.leftAnchor.constraint(equalTo: leftAnchor),
            btnBack.heightAnchor.constraint(equalTo: heightAnchor),
            btnBack.widthAnchor.constraint(equalTo: btnBack.heightAnchor),
            
        ])
        btnBack.addTarget(self, action: #selector(backClicked), for: .touchUpInside)
    }
    
    @objc func backClicked() {
        delegate?.customNavigationBar_BackClick(self)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
}

protocol ViewAdsAndIAPDelegate: class {
    func viewAdsAndIAP_adsClick(_ view: UIView)
    func viewAdsAndIAP_iapClick(_ view: UIView)
}

class ViewAdsAndIAP: UIView {
    let btnAds = UIButton()
    let btnIAP = UIButton()
    
    weak var delegate: ViewAdsAndIAPDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        
        let lbUnlock = UILabel()
        lbUnlock.text = "Unlock themes for 1 minutes"
        lbUnlock.textAlignment = .center
        lbUnlock.font = .boldSystemFont(ofSize: 16)
        lbUnlock.textColor = .black
        let img = UIImageView()
        img.image = UIImage(named: "img_iap")
        img.contentMode = .scaleAspectFit
        
        self.backgroundColor = .white
        self.cornerRadius = 8
        let stack = UIStackView(arrangedSubviews: [img, lbUnlock, btnAds, btnIAP])
        btnIAP.cornerRadius = 8
        btnAds.cornerRadius = 8
        btnIAP.backgroundColor = .black
        btnIAP.setTitle("Go Premium", for: .normal)
        btnIAP.setTitleColor(.white, for: .normal)
        btnAds.backgroundColor = .black
        btnAds.setTitle("Watch an Ad", for: .normal)
        btnAds.setTitleColor(.white, for: .normal)
        stack.axis = .vertical
        stack.spacing = 16
        addSubview(stack)
        stack.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(24)
            make.top.equalToSuperview().offset(16)
        }
        btnIAP.snp.makeConstraints { (make) in
            make.height.equalTo(44)
            make.height.equalTo(btnAds)
        }
        
        img.snp.makeConstraints { (make) in
            make.height.equalTo(80)
        }
        
        let btnX = UIButton()
        btnX.setImage(UIImage(systemName: "Xmark"), for: .normal)
        addSubview(btnX)
        btnX.snp.makeConstraints { (make) in
            make.width.height.equalTo(30)
            make.top.equalToSuperview().offset(4)
            make.right.equalToSuperview().offset(-4)
        }
        btnX.addTapGestureRecognizer { [weak self] in
            guard let self = self else {return}
            self.removeFromSuperview()
        }
        
        btnAds.addTarget(self, action: #selector(btnClicked(sender:)), for: .touchUpInside)
    }
    
    @objc func btnClicked(sender: UIButton) {
        if sender == btnAds {
            delegate?.viewAdsAndIAP_adsClick(self)
        } else {
            delegate?.viewAdsAndIAP_iapClick(self)
        }
    }
    
}
