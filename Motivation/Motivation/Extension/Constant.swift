//
//  Constant.swift
//  Motivation
//
//  Created by hoanglinh on 09/03/2021.
//

import UIKit
import SwiftyJSON
import RealmSwift
import UserNotifications

let IS_IPHONE = (UIDevice.current.userInterfaceIdiom == .phone)
let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad
let screenSize = UIScreen.main.bounds
let SCREEN_MAX_LENGTH = (max(SCREEN_WIDTH, SCREEN_HEIGHT))
let IS_IPHONE_X = (SCREEN_MAX_LENGTH >= 812.0)
let IS_IPHONE_5 = (SCREEN_WIDTH <= 320)

struct Constant {
    
    static let share = Constant()
    
    let defaultCategories: JSON = [
        [
            "category_id": 1,
            "category_name": "Entrepreneurship",
            "del_flg": 0,
            "created_at": nil,
            "updated_at": nil
        ],
        [
            "category_id": 2,
            "category_name": "Mentality",
            "del_flg": 0,
            "created_at": nil,
            "updated_at": nil
        ],
        [
            "category_id": 3,
            "category_name": "Network_Relationships",
            "del_flg": 0,
            "created_at": nil,
            "updated_at": nil
        ],
        [
            "category_id": 4,
            "category_name": "Overcome Hardship",
            "del_flg": 0,
            "created_at": nil,
            "updated_at": nil
        ],
        [
            "category_id": 5,
            "category_name": "Personal Finance",
            "del_flg": 0,
            "created_at": nil,
            "updated_at": nil
        ],
        [
            "category_id": 6,
            "category_name": "Physical Health",
            "del_flg": 0,
            "created_at": nil,
            "updated_at": nil
        ],
        [
            "category_id": 7,
            "category_name": "Realization",
            "del_flg": 0,
            "created_at": nil,
            "updated_at": nil
        ],
        [
            "category_id": 8,
            "category_name": "Relieving",
            "del_flg": 0,
            "created_at": nil,
            "updated_at": nil
        ]
    ]
    
    let defaultThemes = [Asset.theme1, Asset.theme2, Asset.theme3, Asset.theme4, Asset.theme5, Asset.theme1, Asset.theme2, Asset.theme3, Asset.theme4, Asset.theme5, Asset.theme1, Asset.theme2, Asset.theme3, Asset.theme4, Asset.theme5]
    
    func createDefautThemes() {
        let realm = try! Realm()
        for i in 0..<defaultThemes.count {
            let obj = ThemeObject()
            obj.index = i
            if let data = UIImage(asset: defaultThemes[i])?.jpegData(compressionQuality: 100) {
                obj.theme = data
            }
            
            try! realm.write {
                realm.add(obj)
            }
        }
    }
    
    func getThemes() -> [ThemeObject] {
        let realm = try! Realm()
        let themes = realm.objects(ThemeObject.self)
        return Array(themes)
    }
    
    func createCategory() -> [CategoryModel] {
        var cates = [CategoryModel]()
        let decoder = JSONDecoder()
        do {
            Constant.share.getUserDefault().set(try defaultCategories.rawData(), forKey: "categoriesData")
            let cate = try decoder.decode([CategoryModel].self, from: defaultCategories.rawData())
            cates.append(contentsOf: cate)
            return cates
        } catch {
            return cates
        }
    }
    
    func getCategories() -> [CategoryModel] {
        var cates = [CategoryModel]()
        let decoder = JSONDecoder()
        do {
            if let data = Constant.share.getUserDefault().object(forKey: "categoriesData") as? Data {
                let cate = try decoder.decode([CategoryModel].self, from: data)
                cates.append(contentsOf: cate)
                return cates
            } else {
                return cates
            }
        } catch {
            return cates
        }
    }
    
    func getUserDefault() -> UserDefaults {
        if let usd = UserDefaults(suiteName: "group.com.hoan.Motivations.gr") {
            return usd
        }
        return UserDefaults.standard
    }
    
    func getDefaultDataCategory() -> [CategoryObject] {
        guard let path = Bundle.main.path(forResource: "defaultData", ofType:"json") else {
            return [CategoryObject]()
        }
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let decode = JSONDecoder()
            let cate = try decode.decode([CategoryObject].self, from: data)
            return cate
        } catch {
               // handle error
        }
        return [CategoryObject]()
    }
    
    func getDefaultVideo() -> [VideoObject] {
        var objs = [VideoObject]()
        let cate = getDefaultDataCategory()
        for item in cate {
            objs.append(contentsOf: item.video_info)
        }
        return objs
    }
    
    func setupLocalPush() {
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        var numberOfPush = 10
        if let number = getUserDefault().object(forKey: "numberOfPush") as? Int {
            numberOfPush = number*2
        }
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "hh:mm"
        var startDate = Date()
        var endDate = Date()
        if let start = getUserDefault().object(forKey: "startDate") as? Date, let end = getUserDefault().object(forKey: "endDate") as? Date {
            startDate = start
            endDate = end
        } else {
            if let dateStart = dateformatter.date(from: "09:00"), let dateEnd = dateformatter.date(from: "21:00") {
                startDate = dateStart
                endDate = dateEnd
            }
        }
        
        guard let diff = Calendar.current.dateComponents([.minute], from: startDate, to: endDate).minute else {return}
        
        let timeInterval = diff/numberOfPush
        for i in 1..<8 {
            var currentDate = startDate
            for _ in 0..<numberOfPush {
                fireNotificationWith(date: currentDate, weekDay: i)
                currentDate = Calendar.current.date(byAdding: .minute, value: timeInterval, to: currentDate)!
            }
        }
    }
    
    func fireNotificationWith(date: Date, weekDay: Int) {
        
        let body = getDefaultVideo().randomElement()?.quotes ?? "Keep working hard, success will come for you"
        
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "Motivation"
        notificationContent.body = body
        notificationContent.sound = .default
                        
        var datComp = DateComponents()
        datComp.hour = Calendar.current.component(.hour, from: date)
        datComp.minute = Calendar.current.component(.minute, from: date)
        datComp.weekday = weekDay
        print(datComp)
        let trigger = UNCalendarNotificationTrigger(dateMatching: datComp, repeats: true)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: notificationContent, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error : Error?) in
            if let theError = error {
                print(theError)
            }
        }
    }
    
}
