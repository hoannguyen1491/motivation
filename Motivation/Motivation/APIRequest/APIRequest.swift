//
//  APIRequest.swift
//  Motivation
//
//  Created by TopTomsk on 31/03/2021.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

class APIRequest: NSObject {
    static let share = APIRequest()
    
    func baseRequest(url: String, completion: @escaping (_ responseData: Data?, _ error:Error?)->Void) {
        AF.request("\(L10n.Url.base)\(url)", method: .get, parameters: [:], headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                completion(response.data, nil)
            case .failure(let error):
                completion(nil, error)
            }
            
        }
    }
    
    func getListCategory(completion: @escaping (_ responseData: [CategoryModel]?, _ error:Error?)->Void) {
        baseRequest(url: L10n.Url.getCategory) { (data, error) in
            if let `data` = data {
                let decode = JSONDecoder()
                do {
                    Constant.share.getUserDefault().set(data, forKey: "categoriesData")
                    let cates = try decode.decode([CategoryModel].self, from: data)
                    completion(cates, nil)
                } catch (let err) {
                    completion(nil, err)
                }
            } else {
                completion(nil, error)
            }
        }
    }
    
    func getListVideoHome(completion: @escaping (_ responseData: [VideoObject]?, _ error:Error?)->Void) {
        baseRequest(url: L10n.Url.getListVideo) { (data, error) in
            if let `data` = data {
                let decode = JSONDecoder()
                do {
                    let videos = try decode.decode([VideoObject].self, from: data)
                    completion(videos, nil)
                } catch (let err) {
                    print(err)
                    completion(nil, err)
                }
            } else {
                completion(nil, error)
            }
        }
    }
    
    func getListVideoCategories(completion: @escaping (_ responseData: [[VideoObject]]?, _ error:Error?)->Void) {
        baseRequest(url: L10n.Url.getListVideoOfCate) { (data, error) in
            if let `data` = data {
                let decode = JSONDecoder()
                do {
                    let videos = try decode.decode([[VideoObject]].self, from: data)
                    completion(videos, nil)
                } catch (let err) {
                    completion(nil, err)
                }
            } else {
                completion(nil, error)
            }
        }
    }
    
    func getListVideoOfCategory(idCate: Int, completion: @escaping (_ responseData: [VideoObject]?, _ error:Error?)->Void) {
        let urlRequest = L10n.Url.getListVideoOfCate + idCate.description
        baseRequest(url: urlRequest) { (data, error) in
            if let `data` = data {
                let decode = JSONDecoder()
                do {
                    let videos = try decode.decode([VideoObject].self, from: data)
                    completion(videos, nil)
                } catch (let err) {
                    completion(nil, err)
                }
            } else {
                completion(nil, error)
            }
        }    }
    
    func getCategoryVideos(completion: @escaping (_ responseData: Any?, _ error:Error?)->Void) {
        let urlRequest = L10n.Url.base + L10n.Url.getCategoryVideos
        AF.request(urlRequest, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {
            (response) in
            switch response.result {
            case .success(_):
                completion(response.data, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }
    
    
    
    func getRequest(url:String, params: Parameters?, headers: HTTPHeaders?, completion: @escaping (_ responseData: Any?, _ error: Error?)->Void) {
        AF.request(url, method: .get, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
            switch response.result {
            case let .success(value):
                completion(value as? NSDictionary, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }
    
    func likeVideo(obj: VideoObject) {
        let realm = try! Realm()
        do {
            
            let encode = JSONEncoder()
            let data = try encode.encode(obj)
            
            if let objs = realm.object(ofType: RealmVideoLike.self, forPrimaryKey: obj.url.convertToRealURLString()) {
                try realm.write {
                    realm.delete(objs)
                }
            } else {
                let like = RealmVideoLike()
                like.data = data
                like.urlString = obj.url.convertToRealURLString()
                try realm.write {
                    realm.add(like)
                }
            }
        } catch {
        
        }
    }
    
    func getVideoLike(obj: VideoObject) -> Bool {
        
        do {
            let realm = try Realm()
            if let _ = realm.object(ofType: RealmVideoLike.self, forPrimaryKey: obj.url.convertToRealURLString()) {
                return true
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}
