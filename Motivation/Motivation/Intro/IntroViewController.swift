//
//  IntroViewController.swift
//  Motivation
//
//  Created by TopTomsk on 05/03/2021.
//

import UIKit
import GSPlayer

class IntroViewController: UIViewController {

    @IBOutlet weak var lbCate: UILabel!
    @IBOutlet weak var lbQuote: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    let viewStart = ViewGetStart()
    let viewReminder = ViewReminder()
    let viewCate = ViewIntroCategory()
    let viewWidget = ViewIntroWidget()
    let viewIAP = ViewIntroIAP()
    let btnSkip = UIButton()
    
    @IBOutlet weak var playerView: VideoPlayerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        backgroundImage.dimImage()
        
        //Setup action pressbutton
        viewStart.btnGetStarted.addTarget(self, action: #selector(nextClick(sender:)), for: .touchUpInside)
        
        //Play intro video
        playVideo()

        playerView.playToEndTime = { [weak self] in
            guard let `self` = self else {return}
            self.startIntro()
        }
        //Navigate to Intro View, Check xem user đã thực hiện đến bước nào
        Constant.share.getUserDefault().set(IntroStep.kGetStart.rawValue, forKey: "introDone")
    }
    
    func startIntro() {
        self.btnSkip.removeFromSuperview()
        self.viewStart.alpha = 0
        self.playerView.pause(reason: .userInteraction)
        self.viewStart.frame = self.view.frame
        self.viewStart.btnGetStarted.addTarget(self, action: #selector(self.nextClick(sender:)), for: .touchUpInside)
        self.view.addSubview(self.viewStart)
        UIView.animate(withDuration: 1) {
            self.viewStart.alpha = 1
            self.view.layoutIfNeeded()
        } completion: { _ in
            self.viewStart.animatedItems(collectionItemsView: self.viewStart.collectionItem, duration: 0.8)
        }
    }
    
    private func playVideo() {
        playerView.backgroundColor = .black
        playerView.contentMode = .scaleAspectFit
        guard let path = Bundle.main.path(forResource: "videoIntro", ofType:"MOV") else {
            debugPrint("video.mp4 not found")
            return
        }
        playerView.play(for: URL(fileURLWithPath: path))
        
        btnSkip.setTitle("Skip", for: .normal)
        btnSkip.setTitleColor(.lightGray, for: .normal)
        view.addSubview(btnSkip)
        btnSkip.snp.makeConstraints { (make) in
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(-20)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(36)
            make.width.equalTo(80)
        }
        btnSkip.addTapGestureRecognizer { [weak self] in
            guard let `self` = self else {return}
            self.playerView.pause(reason: .userInteraction)
            self.startIntro()
        }
    }
    
    //MARK: - Action click buttons
    @objc func nextClick(sender: UIButton) {
        
        //Play sound when click
        SystemSound.pressClick.play()
        
        switch sender {
        case viewStart.btnGetStarted:   //Next to Reminder
            self.viewReminder.frame = self.view.frame
            viewReminder.btnContinue.addTarget(self, action: #selector(nextClick(sender:)), for: .touchUpInside)
            self.view.addSubviewWithAnimation(subView: self.viewReminder)
            self.viewReminder.animatedItems(collectionItemsView: self.viewReminder.collectionItems)
            self.viewStart.removeFromSuperview()
            Constant.share.getUserDefault().set(IntroStep.kGetStart.rawValue, forKey: "introDone")
            
        case viewReminder.btnContinue:   //Next to Category
            self.viewCate.frame = self.view.frame
            viewCate.btnContinue.addTarget(self, action: #selector(nextClick(sender:)), for: .touchUpInside)
            self.view.addSubviewWithAnimation(subView: self.viewCate)
            self.viewReminder.removeFromSuperview()
            Constant.share.getUserDefault().set(IntroStep.kReminder.rawValue, forKey: "introDone")
            
        case viewCate.btnContinue:   //Next to Widget
            self.viewWidget.frame = self.view.frame
            viewWidget.btnContinue.addTarget(self, action: #selector(nextClick(sender:)), for: .touchUpInside)
            self.view.addSubviewWithAnimation(subView: self.viewWidget)
            self.viewWidget.animatedItems(collectionItemsView: self.viewWidget.collectionItem)
            self.viewCate.removeFromSuperview()
            Constant.share.getUserDefault().set(IntroStep.kCategory.rawValue, forKey: "introDone")
            
        case viewWidget.btnContinue:   //Next to IAP
            self.viewIAP.frame = self.view.frame
            viewIAP.btnContinue.addTarget(self, action: #selector(nextClick(sender:)), for: .touchUpInside)
            viewIAP.btnCancel.addTarget(self, action: #selector(nextClick(sender:)), for: .touchUpInside)
            self.view.addSubviewWithAnimation(subView: self.viewIAP)
            self.viewIAP.animatedItems(collectionItemsView: self.viewIAP.collectionItems)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.viewIAP.animatedItems(collectionItemsView: [self.viewIAP.btnCancel], duration: 1)
            }
            
            self.viewWidget.removeFromSuperview()
            Constant.share.getUserDefault().set(IntroStep.kWidget.rawValue, forKey: "introDone")
            
        case viewIAP.btnCancel:   //Next to Purchase
            let startVC = StartViewController()
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromRight
            transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
            view.window!.layer.add(transition, forKey: kCATransition)
            let nav = NavigationController(rootViewController: startVC)
            nav.navigationController?.navigationBar.isHidden = true
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: false, completion: nil)
        default:
            break
        }
    }
    
}
