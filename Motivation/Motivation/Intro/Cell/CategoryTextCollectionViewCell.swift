//
//  CategoryTextCollectionViewCell.swift
//  Motivation
//
//  Created by TopTomsk on 05/03/2021.
//

import UIKit

protocol CategoryDelegate: class {
    func didSelectCellCategory(indexPath: IndexPath)
}

class CategoryTextCollectionViewCell: UICollectionViewCell {
    
    weak var delegate: CategoryDelegate?
    var indexPath: IndexPath = IndexPath()
    var isAnimated = false
    
    var labelText: UIButton = UIButton(type: .system)
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //Setup constraint and setting label
        addSubview(labelText)
        labelText.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            labelText.centerXAnchor.constraint(equalTo: centerXAnchor),
            labelText.centerYAnchor.constraint(equalTo: centerYAnchor),
            labelText.topAnchor.constraint(equalTo: topAnchor),
            labelText.leftAnchor.constraint(equalTo: leftAnchor)
        ])
        labelText.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 16)
        labelText.addTarget(self, action: #selector(actionSelected), for: .touchUpInside)
        labelText.setTitleColor(.black, for: .normal)
        labelText.backgroundColor = UIColor(rgb: 0xF5EBE5)
        labelText.cornerRadius = 22
    }

    var isSelectedCell = false

    @objc func actionSelected() {
        //Play sound
        SystemSound.pressClick.play()
        
        //Setting cell selected
        if isSelectedCell {
            labelText.setTitleColor(.black, for: .normal)
            labelText.backgroundColor = UIColor(rgb: 0xF5EBE5)
        } else {
            labelText.setTitleColor(.white, for: .normal)
            labelText.backgroundColor = .black
        }
        isSelectedCell = !isSelectedCell
        
        //Send delegate to ViewController
        delegate?.didSelectCellCategory(indexPath: indexPath)
    }
    
}
