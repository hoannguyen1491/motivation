//
//  ViewIntroCategory.swift
//  Motivation
//
//  Created by TopTomsk on 05/03/2021.
//

import UIKit

class ViewIntroCategory: UIView {

    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let model = CategoryViewModel()
    
    //Fake data cateegory
    var items = [CategoryModel]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(L10n.Text.viewIntroCategory, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        //Register Cell
        collectionView.register(UINib(nibName: L10n.Text.categoryTextCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: L10n.Text.categoryTextCollectionViewCell)
        collectionView.delaysContentTouches = false
        
        //Get categories from server
        model.getCategories()
        
        //Received categories from sever: reload
        model.didFinishFetched = { [weak self] in
            guard let `self` = self else {return}
            self.items = self.model.categories
            self.collectionView.reloadData()
        }
        
        //Error or something wrong: Tạo category default
        model.showAlertClosure = { [weak self] in
            guard let `self` = self else {return}
            self.items = Constant.share.createCategory()
            self.collectionView.reloadData()
        }
        
    }
    
    private func createCategory() {
        
    }
    
}

//MARK: - CollectionView Datasource
extension ViewIntroCategory: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: L10n.Text.categoryTextCollectionViewCell, for: indexPath) as! CategoryTextCollectionViewCell
        
        //Set title label text
        cell.labelText.setTitle(items[indexPath.item].category_name, for: .normal)
        
        //Set animation cell
        let transition: UIView.AnimationOptions = indexPath.row % 2 == 0 ? .transitionFlipFromLeft : .transitionFlipFromRight
        if !cell.isAnimated {
            let delaytime = 0.6 * Double(indexPath.row)
            UIView.animate(withDuration: 0.6,
                           delay: delaytime,
                           usingSpringWithDamping: 1,
                           initialSpringVelocity: 0.6,
                           options: transition,
                           animations: {

                //Nếu cell chẵn thì animated từ bên trái vào, lẻ thì từ phải vào
                if indexPath.row % 2 == 0 {
                    ViewIntroCategory.viewSlideInFromLeft(toRight: cell)
                } else {
                    ViewIntroCategory.viewSlideInFromRight(toLeft: cell)
                }
            }, completion: { (done) in
                cell.isAnimated = true
            })
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //Set size of cell (padding 16*2 + spacing 10)
        let size = (UIScreen.main.bounds.width - 42)/2
        return .init(width: size, height: 44)
    }
    
//MARK: - Animation Cell from left and right
    static let kSlideAnimationDuration: CFTimeInterval = 0.6

    static func viewSlideInFromRight(toLeft views: UIView) {
        var transition: CATransition? = nil
        transition = CATransition.init()
        transition?.duration = kSlideAnimationDuration
        transition?.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition?.type = CATransitionType.push
        transition?.subtype = CATransitionSubtype.fromRight
        views.layer.add(transition!, forKey: nil)
    }

    static func viewSlideInFromLeft(toRight views: UIView) {
        var transition: CATransition? = nil
        transition = CATransition.init()
        transition?.duration = kSlideAnimationDuration
        transition?.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition?.type = CATransitionType.push
        transition?.subtype = CATransitionSubtype.fromLeft
        views.layer.add(transition!, forKey: nil)
    }
}
