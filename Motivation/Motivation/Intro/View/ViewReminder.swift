//
//  ViewReminder.swift
//  Motivation
//
//  Created by TopTomsk on 05/03/2021.
//

import UIKit

class ViewReminder: UIView {
    
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnIncrease: UIButton!
    @IBOutlet weak var btnDecrease: UIButton!
    @IBOutlet var collectionItems: [UIView]!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lbNumberReminder: UILabel!
    @IBOutlet weak var lbTimeStart: UILabel!
    @IBOutlet weak var lbTimeEnd: UILabel!
    @IBOutlet weak var viewStartTime: UIView!
    @IBOutlet weak var viewEndTime: UIView!
    
    let datePicker = UIDatePicker()
    
    var currentNumberReminder: Int = 10 {
        didSet {
            //Update label
            lbNumberReminder.text = currentNumberReminder.description + "x"
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    var isStartTime = true
    var isShowDatePicker = false
    
    func commonInit() {
        Bundle.main.loadNibNamed(L10n.Text.viewReminder, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        datePicker.datePickerMode = .time
        datePicker.backgroundColor = .white
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        
        let bg = UIView()
        bg.backgroundColor = .white
        addSubview(bg)
        addSubview(datePicker)
        datePicker.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.snp.bottom).offset(44)
            make.height.equalTo(200)
        }
        bg.snp.makeConstraints { (make) in
            make.edges.equalTo(datePicker)
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.items = [
        UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPickerView)),
        UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
        UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePickerView))]
        addSubview(toolBar)
        
        toolBar.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(44)
            make.bottom.equalTo(datePicker.snp.top)
        }
        
        viewStartTime.isUserInteractionEnabled = true
        viewEndTime.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(timeGestureClicked(sender:)))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(timeGestureClicked(sender:)))
        viewStartTime.addGestureRecognizer(tap)
        viewEndTime.addGestureRecognizer(tap2)
        
        
    }
    
    @objc func cancelPickerView() {
        UIView.animate(withDuration: 0.5) {
            self.datePicker.snp.remakeConstraints { (make) in
                make.left.right.equalToSuperview()
                make.top.equalTo(self.snp.bottom).offset(44)
                make.height.equalTo(200)
            }
            self.layoutIfNeeded()
        }
        isShowDatePicker = false
    }
    
    @objc func donePickerView() {
        UIView.animate(withDuration: 0.5) {
            self.datePicker.snp.remakeConstraints { (make) in
                make.left.right.equalToSuperview()
                make.top.equalTo(self.snp.bottom).offset(44)
                make.height.equalTo(200)
            }
            self.layoutIfNeeded()
        }
        isShowDatePicker = false
        
        //Logic update value
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm"
        let string = dateFormatter.string(from: datePicker.date)
        if isStartTime {
            lbTimeStart.text = string
            Constant.share.getUserDefault().set(datePicker.date, forKey: "startDate")
        } else {
            lbTimeEnd.text = string
            Constant.share.getUserDefault().set(datePicker.date, forKey: "endDate")
        }
    }
    
    @objc func timeGestureClicked(sender: UITapGestureRecognizer) {
        isStartTime = sender.view == viewStartTime ? true : false
        
        UIView.animate(withDuration: 0.5) {
            if !self.isShowDatePicker {
                self.datePicker.snp.remakeConstraints { (make) in
                    make.left.right.equalToSuperview()
                    make.bottom.equalTo(self.snp.bottom)
                    make.height.equalTo(200)
                }
            } else {
                self.datePicker.snp.remakeConstraints { (make) in
                    make.left.right.equalToSuperview()
                    make.top.equalTo(self.snp.bottom).offset(44)
                    make.height.equalTo(200)
                }
            }
            self.layoutIfNeeded()
        }
        isShowDatePicker = !isShowDatePicker
    }
    
    @IBAction func actionNumberReminder(_ sender: UIButton) {
        //Vibrate when click button
        SystemSound.pressClick.play()
        
        //New value number reminder per day
        let newValue = currentNumberReminder + (sender.tag == 0 ? -1 : 1)
        if newValue >= 1 && newValue <= 20 {
            // Check transition animated (decrease -> from bottom, increase -> fromtop)
            let transition = sender.tag == 0 ? CATransitionSubtype.fromBottom : CATransitionSubtype.fromTop
            lbNumberReminder.pushTransition(0.3, from: transition)
            
            //Set new Value
            currentNumberReminder = newValue
        }
    }
}
