//
//  ViewIntroWidget.swift
//  Motivation
//
//  Created by TopTomsk on 08/03/2021.
//

import UIKit

class ViewIntroWidget: UIView {

    @IBOutlet var collectionItem: [UIView]!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var btnContinue: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(L10n.Text.viewIntroWidget, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
}
