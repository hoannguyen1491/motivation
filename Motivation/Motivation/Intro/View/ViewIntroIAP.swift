//
//  ViewIntroIAP.swift
//  Motivation
//
//  Created by TopTomsk on 08/03/2021.
//

import UIKit
import StoreKit

class ViewIntroIAP: UIView {

    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet var collectionItems: [UIView]!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var btnContinue: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(L10n.Text.viewIntroIAP, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.frame
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        btnContinue.addTarget(self, action: #selector(purchase), for: .touchUpInside)
        
    }
    
    @objc func purchase() {
        IAPObject.share.fetchAvailableProducts()
        if let firstProduct = IAPObject.share.iapProducts.first {
            IAPObject.share.buyProduct(firstProduct)
        }
    }
    
}

class IAPObject: NSObject {
    
    static let share = IAPObject()
    
    var productIdentifier = "xxx.xxx.sampleProductId" //Lay tu itunes connect
    var productID = ""
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    
    
    func fetchAvailableProducts() {
         // Put here your IAP Products ID's
        let productIdentifiers = NSSet(objects: productIdentifier)
        guard let identifier = productIdentifiers as? Set<String> else { return }
        productsRequest = SKProductsRequest(productIdentifiers: identifier)
        productsRequest.delegate = self
        productsRequest.start()
    }
    
    public func buyProduct(_ product: SKProduct) {
      print("Mua \(product.productIdentifier)...")
      let payment = SKPayment(product: product)
      SKPaymentQueue.default().add(payment)
    }
    
}
extension IAPObject: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if response.products.count > 0 {
            iapProducts = response.products
        }
    }
}
extension IAPObject: SKPaymentTransactionObserver {
 
    public func paymentQueue(_ queue: SKPaymentQueue,
                           updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                print("success")
                break
            case .failed:
                print("failed")
                break
            case .restored:
                print("restore")
                break
            case .deferred:
                break
            case .purchasing:
                break
            @unknown default:
                fatalError()
            }
        }
    }
}
