//
//  SceneDelegate.swift
//  Motivation
//
//  Created by TopTomsk on 05/03/2021.
//

import UIKit
import Firebase
import GoogleMobileAds
import AVKit
import RealmSwift
import UserNotifications
import WidgetKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback, options: .defaultToSpeaker)
        } catch {
            print("Unable to set audio session category")
        }
        
        
        let realm = try! Realm()
        let themes = realm.objects(ThemeObject.self)
        if themes.count == 0 {
            Constant.share.createDefautThemes()
        }
        
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { _, _ in
                Constant.share.setupLocalPush()
            })
        
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        
        Thread.sleep(forTimeInterval: 1.0)
        
        if let _ = Constant.share.getUserDefault().object(forKey: "introDone") as? Int {
            let nav = NavigationController(rootViewController: HomePageViewController())
            nav.navigationController?.navigationBar.isHidden = true
            window?.rootViewController = nav
        } else {
            window?.rootViewController = IntroViewController()
        }
        FirebaseApp.configure()
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["6c99bb039ff52ac3fecaaaf3d70ee249"]
                
        window?.makeKeyAndVisible()
        
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        let videos = Constant.share.getDefaultVideo()
        Constant.share.getUserDefault().set(videos.randomElement()?.quotes, forKey: "quoteWidget")
        if #available(iOS 14.0, *) {
            WidgetCenter.shared.reloadAllTimelines()
        } else {
            // Fallback on earlier versions
        }
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("vvv")
    }
    
}

