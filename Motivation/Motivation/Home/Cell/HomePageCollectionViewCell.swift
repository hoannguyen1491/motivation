//
//  HomePageCollectionViewCell.swift
//  Motivation
//
//  Created by hoanglinh on 05/03/2021.
//

import UIKit
import GSPlayer
import AVKit
import GradientProgressBar
import DOFavoriteButtonNew
import Photos

protocol HomePageCollectionViewCellDelegate: class {
    func homePageCollectionViewCell_RequestSettingPrivacy(_ cell: UIView)
    func homePageCollectionViewCell_CategoryClicked(_ cell: UIView, id: Int)
}

class HomePageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var likeButton: DOFavoriteButtonNew!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var desLabel: VerticalTopAlignLabel!
    @IBOutlet weak var titleButton: UIButton!
    
    @IBOutlet weak var likeView: UIView!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var stackViewDownload: UIStackView!
    @IBOutlet weak var stackViewShare: UIStackView!
    @IBOutlet weak var icDownload: UIImageView!
    @IBOutlet weak var icShare: UIImageView!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var downloadLabel: UILabel!
    @IBOutlet weak var bottomCstPlayerView: NSLayoutConstraint!
    
    @IBOutlet weak var playerView: VideoPlayerView!
    var url: URL?
    @IBOutlet weak var progressBar: GradientProgressBar!
    @IBOutlet weak var bottomCstProgress: NSLayoutConstraint!
    
    let progressBarPlayer = GradientProgressBar()
    
    weak var delegate: HomePageCollectionViewCellDelegate?
    
    var timer: Timer?
    var viewModel = HomeCellViewModel()
    var data: VideoObject?
    
    var currentBGImg: UIImage? {
        didSet {
            self.bgImageView.image = currentBGImg
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        playerView.setUpNotification()
        progressBar.tintColor = .clear
        progressBar.backgroundColor = .clear
        self.progressBarPlayer.backgroundColor = .clear
        self.progressBarPlayer.gradientColors = [.red,.red,.red]
        
        if IS_IPHONE_X {
            bottomCstProgress.constant = 8
        }
        bgImageView.contentMode = .scaleAspectFit
        self.btnPlay.alpha = 0.7
        btnPlay.addTarget(self, action: #selector(pauseAndPlayDidTap), for: .touchUpInside)
        self.btnPlay.isHidden = true
        
        // Player state change
        playerView.stateDidChanged = { [weak self] state in
            guard let `self` = self else {return}
            switch state {
            case .none:
                print("none")
            case .error(let error):
                print("error - \(error.localizedDescription)")
            case .loading:
                print("loading")
            case .paused:
                self.btnPlay.isHidden = false
            case .playing:
                self.btnPlay.isHidden = true
                //Setup time bar observer
                if self.timer == nil {
                    self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.observeTimer), userInfo: nil, repeats: true)
                }
            }
        }
        
        //Setup Like Btn
        likeButton.imageColorOff = UIColor.white
        likeButton.imageColorOn = UIColor.red
        likeButton.circleColor = UIColor.red
        likeButton.lineColor = UIColor.red
        likeButton.duration = 1.5
        shareButton.imageView?.contentMode = .scaleAspectFit
        likeButton.imageView?.contentMode = .scaleAspectFit
        
        //Setup action
        likeButton.addTarget(self, action: #selector(likeButtonDidTap(sender:)), for: .touchUpInside)
        shareButton.addTarget(self, action: #selector(shareButtonDidTap), for: .touchUpInside)
        categoryButton.addTarget(self, action: #selector(categoryButtonDidTap), for: .touchUpInside)
        
        //Download click
        stackViewDownload.addTapGestureRecognizer { [weak self] in
            guard let `self` = self, let url = self.url else {return}
            if self.checkPhotoLibraryPermission() == .notDetermined || self.checkPhotoLibraryPermission() == .authorized {
                self.shareView.fadeOut(0.3)
                self.viewModel.downloadVideo(url: url)
            } else {
                self.delegate?.homePageCollectionViewCell_RequestSettingPrivacy(self)
            }
        }
        
        //Setup property title,des
        titleButton.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        titleButton.setTitleColor(.white, for: .normal)
        titleButton.backgroundColor = #colorLiteral(red: 0, green: 0.7807711959, blue: 0.636285007, alpha: 1)
        titleButton.cornerRadius = titleButton.frame.height/2
        
        titleButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        desLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        desLabel.textColor = .white
        
        //Setup tap/doubleTap action
        playerView.isUserInteractionEnabled = true
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(singleTapGesture))
        let doubleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(doubletap))
        doubleTap.numberOfTapsRequired = 2
        playerView.addGestureRecognizer(singleTap)
        playerView.addGestureRecognizer(doubleTap)
        singleTap.require(toFail: doubleTap)
        singleTap.delaysTouchesBegan = true
        doubleTap.delaysTouchesBegan = true
        
        //Download video setup
        viewModel.progress = { [weak self] value in
            guard let `self` = self else {return}
            self.progressBar.setProgress(Float(value), animated: true)
        }
        viewModel.didFinishDownloaded = { [weak self] in
            guard let `self` = self else {return}
            DispatchQueue.main.async {
                self.progressBar.progress = 0
            }
        }
        
    }
    
    func setDataToCell(data: VideoObject) {
        
        self.progressBarPlayer.progress = 0
        
        //Check if video like or not
        if APIRequest.share.getVideoLike(obj: data) == true {
            likeButton.select()
        } else {
            likeButton.deselect()
        }
        
        self.data = data
        self.bottomCstPlayerView.constant = 100
        self.url = data.url.convertToRealURLString().convertToURL()
        
        //Load cover video
        DispatchQueue.global(qos: .background).async {
            guard let url = self.url else {return}
            HomePageCollectionViewCell.imageFromVideo(url: url, at: 0) { (image) in
                guard let `image` = image else {return}
                DispatchQueue.main.async {
                    self.bgImageView.image = image
                }
            }
        }
        
        shareView.isHidden = true
                
        //Display data
        titleButton.setTitle(data.category_name.uppercased(), for: .normal)
        desLabel.text = data.quotes.uppercased()
    }
    
    //Check resolution video
    func setupContentMode(url: URL) {
        DispatchQueue.global(qos: .background).async {
            if let ratio = self.getVideoResolution(url: url) {
                DispatchQueue.main.async {
                    self.playerView.contentMode = ratio > 1.4 ? .scaleAspectFill : .scaleAspectFit
                    self.bgImageView.contentMode = ratio > 1.4 ? .scaleAspectFill : .scaleAspectFit
                }
            }
        }
    }
        
    func getVideoResolution(url: URL) -> CGFloat? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        
        return abs(size.height) / abs(size.width)
    }
    
    @objc func singleTapGesture() {
        self.pauseAndPlayDidTap()
    }
    
    @objc func doubletap() {
        likeButtonDidTap(sender: self.likeButton)
    }
    
    //Action like, share, category button
    @objc func likeButtonDidTap(sender: DOFavoriteButtonNew) {
        APIRequest.share.likeVideo(obj: self.data!)
        if sender.isSelected {
            // deselect
            sender.deselect()
        } else {
            // select with animation
            sender.select()
        }
    }
    @objc func shareButtonDidTap() {
        if self.shareView.isHidden == true {
            self.shareView.fadeIn(0.3)
        } else {
            self.shareView.fadeOut(0.3)
        }
    }
    
    @objc func categoryButtonDidTap() {
        guard let id = self.data?.category_id else {return}
        delegate?.homePageCollectionViewCell_CategoryClicked(self, id: id)
    }
    
    //Play, pause video
    @objc func pauseAndPlayDidTap() {
        if shareView.isHidden == false {
            shareButtonDidTap()
        } else {
            if playerView.state == .playing {
                playerView.pause(reason: .userInteraction)
            } else {
                playerView.resume()
            }
        }
    }
    
    //Get cover video function
    static func imageFromVideo(url: URL, at time: TimeInterval, completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global(qos: .background).async {
            
            let request = URLRequest(url: url)
            let cache = URLCache.shared

            if
                let cachedResponse = cache.cachedResponse(for: request),
                let image = UIImage(data: cachedResponse.data)
            {
                completion(image)
            }

            let asset = AVAsset(url: url)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true

            var image: UIImage?
            let cmTime = CMTime(seconds: time, preferredTimescale: 60)

            do {
                let cgImage = try imageGenerator.copyCGImage(at: cmTime, actualTime: nil)
                image = UIImage(cgImage: cgImage)
            } catch { }

            if let `image` = image, let data = image.jpegData(compressionQuality: 1.0), let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
            {
                let cachedResponse = CachedURLResponse(response: response, data: data)

                cache.storeCachedResponse(cachedResponse, for: request)
            }

            completion(image)
        }
    }
    
    //Reset cell when reuse
    override func prepareForReuse() {
        super.prepareForReuse()
        playerView.player?.replaceCurrentItem(with: nil)
        playerView.playerURL = nil
        progressBarPlayer.removeFromSuperview()
        self.progressBarPlayer.progress = 0
        timer?.invalidate()
        timer = nil
    }
    
    //Setup time bar
    func setupBar() {
        DispatchQueue.global(qos: .background).async {
            guard let url = self.url else {return}
            if let ratio = self.getVideoResolution(url: url) {
                DispatchQueue.main.async {
                    self.addSubview(self.progressBarPlayer)
                    self.progressBarPlayer.alpha = 1
                    self.progressBarPlayer.snp.makeConstraints { (make) in
                        make.left.right.equalToSuperview()
                        make.height.equalTo(2)
                        make.top.equalTo(self.playerView.snp.centerY).offset(SCREEN_WIDTH*ratio/2)
                    }
                    if ratio > 1.2 {
                        self.bottomCstPlayerView.constant = 0
                    }
                }
            }
        }
    }
    
    //Update time bar
    @objc func observeTimer() {
        progressBarPlayer.progress = Float(playerView.currentDuration/playerView.totalDuration)
    }
    
    func play(url: URL) {
        self.playerView.play(for: url)
        self.setupBar()
    }
    
    func pause() {
        playerView.pause(reason: .hidden)
    }
    
    func checkPhotoLibraryPermission() -> PHAuthorizationStatus {
        let status = PHPhotoLibrary.authorizationStatus()
        return status
    }
}
