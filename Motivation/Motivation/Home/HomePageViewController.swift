//
//  HomePageViewController.swift
//  Motivation
//
//  Created by hoanglinh on 05/03/2021.
//

import UIKit
import GSPlayer
import AVKit

class HomePageViewController: UIViewController {

    @IBOutlet weak var homeCollectionView: UICollectionView!
    @IBOutlet weak var pipBtn: UIButton!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var categoryButton: UIButton!
    
    let model = VideosViewModel()
    var currentIndex: Int = 0
    var items: [VideoObject] = [VideoObject]()
    var pictureInPictureController: AVPictureInPictureController?
    var idCategory = -1 {
        didSet {
            //Gọi API get list videos
            model.getVideos(idCate: self.idCategory)
        }
    }
    var videoID: Int = -1
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true

        // Do any additional setup after loading the view.
        setupView()
        addSwipeAction()
        setupGuide()
        handleViewModelCallback()
        
    }
    
    private func handleViewModelCallback() {
        //Api đã trả về data
        self.idCategory = -1
        model.didFinishFetched = { [weak self] in
            guard let `self` = self else {return}
            self.items = self.model.videos
            self.items.shuffle()
            if let obj = self.items.filter({$0.video_id == self.videoID}).first {
                self.items.bringToFront(item: obj)
            }
            self.homeCollectionView.reloadData { [weak self] in
                guard let `self` = self else {return}
                self.preloadVideosAndPlay()
            }
        }
        // Không lấy đc data, parse data bị lỗi
        model.showAlertClosure = { [weak self] in
            guard let `self` = self else {return}
            if self.items.count == 0 {
                self.items = Constant.share.getDefaultVideo()
                self.items.shuffle()
                self.homeCollectionView.reloadData { [weak self] in
                    guard let `self` = self else {return}
                    self.preloadVideosAndPlay()
                }
            }
        }
    }
    
    private func setupView() {
        self.view.backgroundColor = .black
        settingButton.addTarget(self, action: #selector(settingButtonDidTap), for: .touchUpInside)
        categoryButton.addTarget(self, action: #selector(categoryButtonDidTap), for: .touchUpInside)
        
        homeCollectionView.isPrefetchingEnabled = true
        homeCollectionView.register(UINib(nibName: L10n.Text.homePageCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: L10n.Text.homePageCollectionViewCell)
        homeCollectionView.backgroundColor = .clear
        homeCollectionView.isPagingEnabled = true
        homeCollectionView.contentInsetAdjustmentBehavior = .never
        homeCollectionView.prefetchDataSource = self
        
        //Setup pip button
        let startImage = AVPictureInPictureController.pictureInPictureButtonStartImage
        let stopImage = AVPictureInPictureController.pictureInPictureButtonStopImage
        
        pipBtn.setImage(startImage, for: .normal)
        pipBtn.setImage(stopImage, for: .selected)
        pipBtn.isHidden = !AVPictureInPictureController.isPictureInPictureSupported()
        
        NotificationCenter.default.addObserver(self, selector: #selector(becomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    func setupGuide() {
        
        enum Guideline: Int {
            case swipeUp
            case swipeRight
            case swipeLeft
            case singleTouch
            case doubleTouch
        }
        
        //Check if user did pass guideline
        if Constant.share.getUserDefault().object(forKey: "guideline") != nil {
            return
        }
        
        Constant.share.getUserDefault().set(1, forKey: "guideline")
        
        //Add background guideline
        let viewGuideline = UIView()
        viewGuideline.backgroundColor = UIColor(white: 0, alpha: 0.7)
        view.addSubview(viewGuideline)
        viewGuideline.tag = Guideline.swipeUp.rawValue
        
        viewGuideline.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        //Add image guideline
        let imageGuide = UIImageView()
        view.addSubview(imageGuide)
        imageGuide.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(30)
            make.height.equalTo(200)
        }
        imageGuide.image = UIImage(named: Asset.swipeUp.name)
        imageGuide.contentMode = .scaleAspectFit
        
        //Action user click
        viewGuideline.addTapGestureRecognizer {
            switch viewGuideline.tag {
            case Guideline.swipeUp.rawValue:
                imageGuide.image = UIImage(asset: Asset.swipeRight)
                viewGuideline.tag = Guideline.swipeRight.rawValue
            case Guideline.swipeRight.rawValue:
                imageGuide.image = UIImage(asset: Asset.swipeLeft)
                viewGuideline.tag = Guideline.swipeLeft.rawValue
            case Guideline.swipeLeft.rawValue:
                imageGuide.image = UIImage(asset: Asset.singleTouch)
                viewGuideline.tag = Guideline.singleTouch.rawValue
            case Guideline.singleTouch.rawValue:
                imageGuide.image = UIImage(asset: Asset.doubleTouch)
                viewGuideline.tag = Guideline.doubleTouch.rawValue
            default:
                imageGuide.removeFromSuperview()
                viewGuideline.removeFromSuperview()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        preloadVideosAndPlay()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopVideo()
    }
    
    //Get display cell
    private func getCurrentCell() -> HomePageCollectionViewCell? {
        let center = self.view.convert(homeCollectionView.center, to: homeCollectionView)
        if let index = homeCollectionView.indexPathForItem(at: center) {
            if let cell = homeCollectionView.cellForItem(at: index) as? HomePageCollectionViewCell {
                return cell
            }
        }
        return nil
    }
    
    //Thêm action swipe trái/phải
    func addSwipeAction() {
        self.view.isUserInteractionEnabled = true
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)

        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeDown.direction = .left
        self.view.addGestureRecognizer(swipeDown)
    }
    
    @objc func respondToSwipeGesture(sender: UIGestureRecognizer) {
        stopVideo()
        if let swipeGesture = sender as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case .right:
                let vc = CategoryViewController()
                vc.delegate = self
                vc.isPush = true
                vc.getDataCategorys()
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = CATransitionType.push
                transition.subtype = CATransitionSubtype.fromLeft
                transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
                view.window!.layer.add(transition, forKey: kCATransition)
                vc.modalTransitionStyle = .crossDissolve
                self.navigationController?.pushViewController(vc, animated: true)
            case .left:
                let vc = DetailCategoryViewController()
                vc.delegate = self
                vc.idCate = currentIndex
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                break
            }
        }
    }
    
    //Navigate to Category Viewcontroller
    @objc func categoryButtonDidTap() {
        stopVideo()
        let vc = CategoryViewController()
        vc.delegate = self
        vc.getDataCategorys()
        vc.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    //Navigate to Setting Viewcontroller
    @objc func settingButtonDidTap() {
        stopVideo()
        let vc = SettingViewController()
        vc.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    //Action PictureInPicture
    @IBAction func pipAction(_ sender: Any) {
        if let cell = getCurrentCell(), let pip = cell.playerView.pictureInPictureController {
            pip.delegate = self
            if pip.isPictureInPictureActive {
                pip.stopPictureInPicture()
                pipBtn.isSelected = false
                self.preloadVideosAndPlay()
            } else {
                pip.startPictureInPicture()
                pipBtn.isSelected = true
            }
        }
    }
    
}

//MARK: - CollectionView
extension HomePageViewController: UICollectionViewDataSource, UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: L10n.Text.homePageCollectionViewCell, for: indexPath) as! HomePageCollectionViewCell
        
        //Setdata
        cell.setDataToCell(data: items[indexPath.row])
        cell.delegate = self
        
        //Check if player did end
        cell.playerView.playToEndTime = { [weak self] in
            guard let `self` = self else {return}
            if let val = Constant.share.getUserDefault().object(forKey: "autoNext") as? Bool, val == true {
                UIView.animate(withDuration: 0.5) {
                    self.homeCollectionView.setContentOffset(.init(x: 0, y: self.homeCollectionView.contentOffset.y + SCREEN_HEIGHT), animated: false)
                    self.view.layoutIfNeeded()
                } completion: { _ in
                    self.preloadVideosAndPlay()
                }
            }
        }
        
        cell.stackViewShare.addTapGestureRecognizer {
            if let myWebsite = cell.url {
                let objectsToShare = [myWebsite] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

                //New Excluded Activities Code
                activityVC.excludedActivityTypes =  [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]

                activityVC.popoverPresentationController?.sourceView = nil
                self.present(activityVC, animated: true, completion: nil)
                cell.shareButtonDidTap()
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
//        VideoPreloadManager.shared.set(waiting: indexPaths.map { items[$0.row].url.convertToRealURLString().convertToURL() })
    }
    
    func collectionView(_ collectionView: UICollectionView, cancelPrefetchingForItemsAt indexPaths: [IndexPath]) {
//        for idp in indexPaths {
//            if let url = items[idp.row].url.convertToRealURLString().convertToURL() {
//                VideoPreloadManager.shared.remove(url: url)
//            }
//        }
    }
    
}

extension HomePageViewController:  UICollectionViewDelegate {
    
    //Size of collectionviewcell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width , height: UIScreen.main.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? HomePageCollectionViewCell {
            cell.btnPlay.isHidden = true
        }
    }
    
    //Pause video when cell end display
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? HomePageCollectionViewCell {
            cell.pause()
        }
    }
    
    //MARK: - Scroll delegate to preload and play video
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            print("end dragging")
            preloadVideosAndPlay()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("end decelerating")
        preloadVideosAndPlay()
    }

    func preloadVideosAndPlay() {
        checkPreload()
        checkPlay()
    }
    
    func checkPreload() {
        print("cache size \(VideoCacheManager.calculateCachedSize())")
        guard let lastRow = homeCollectionView.indexPathsForVisibleItems.last?.row else { return }
        let urls = items.compactMap {URL(string: $0.url.convertToRealURLString())}
            .suffix(from: min(lastRow + 1, items.count))
            .prefix(2)
        VideoPreloadManager.shared.set(waiting: Array(urls))
    }

    func checkPlay() {
        let center = self.view.convert(homeCollectionView.center, to: homeCollectionView)
        if let index = homeCollectionView.indexPathForItem(at: center) {
            if let cell = homeCollectionView.cellForItem(at: index) as? HomePageCollectionViewCell, let url = items[index.row].url.convertToRealURLString().convertToURL() {
                currentIndex = items[index.row].category_id
                cell.play(url: url)
                if let pip = cell.playerView.pictureInPictureController {
                    pip.delegate = self
                }
            }
        }
    }
    
    func stopVideo() {
        if let cell = getCurrentCell() {
            if let pip = cell.playerView.pictureInPictureController {
                if !pip.isPictureInPictureActive {
                    cell.playerView.pause(reason: .userInteraction)
                }
            } else {
                cell.playerView.pause(reason: .userInteraction)
            }
        }
    }
    
    @objc func becomeActive() {
        pipBtn.isSelected = false
        getCurrentCell()?.playerView.setupPictureInPicture()
    }
    
    @objc func resignActive() {
        
    }
}

extension HomePageViewController: UICollectionViewDelegateFlowLayout {
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .zero
    }
}

// PIP delegate
extension HomePageViewController: AVPictureInPictureControllerDelegate {
    public func pictureInPictureController(_ pictureInPictureController: AVPictureInPictureController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        // Nếu đang ở màn khác thì pop về màn hình home
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomePageViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        //Reset pip và play video
        self.getCurrentCell()?.playerView.setupPictureInPicture()
        self.preloadVideosAndPlay()
        self.pipBtn.isSelected = false
        completionHandler(true)
    }
    
    func pictureInPictureControllerWillStartPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
        
    }

    func pictureInPictureControllerDidStopPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
        
        // Pause video
        self.pipBtn.isSelected = false
        self.getCurrentCell()?.playerView.setupPictureInPicture()
        self.getCurrentCell()?.playerView.pause(reason: .hidden)
    }
}

// Reload after select category
extension HomePageViewController: CategoryViewControllerDelegate {
    func categoryViewController(_ viewController: UIViewController, categoryIdClick id: Int, videoID: Int) {
        self.videoID = videoID
        self.idCategory = id
    }
}

//MARK: - CollectionViewCell Delegate
extension HomePageViewController: HomePageCollectionViewCellDelegate {

    func homePageCollectionViewCell_CategoryClicked(_ cell: UIView, id: Int) {
        self.stopVideo()
        let detailCateVC = DetailCategoryViewController()
        detailCateVC.idCate = id
        detailCateVC.delegate = self
        detailCateVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(detailCateVC, animated: true)
    }
    
    func homePageCollectionViewCell_RequestSettingPrivacy(_ cell: UIView) {
        let alertController = UIAlertController (title: "Privacy", message: "Go to setting to change privacy ?", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in

            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }
}
