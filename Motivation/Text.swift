// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  internal enum Key {
  }
  internal enum Text {
    /// All
    internal static let all = L10n.tr("Text", "All")
    /// CategoryHeaderView
    internal static let categoryHeaderView = L10n.tr("Text", "CategoryHeaderView")
    /// CategoryHorizontalCell
    internal static let categoryHorizontalCell = L10n.tr("Text", "CategoryHorizontalCell")
    /// CategoryTextCollectionViewCell
    internal static let categoryTextCollectionViewCell = L10n.tr("Text", "CategoryTextCollectionViewCell")
    /// DefaultTableviewCell
    internal static let defaultTableviewCell = L10n.tr("Text", "DefaultTableviewCell")
    /// DetailWidgetTableViewCell
    internal static let detailWidgetTableViewCell = L10n.tr("Text", "DetailWidgetTableViewCell")
    /// HomePageCollectionViewCell
    internal static let homePageCollectionViewCell = L10n.tr("Text", "HomePageCollectionViewCell")
    /// ItemCateHorizontalCell
    internal static let itemCateHorizontalCell = L10n.tr("Text", "ItemCateHorizontalCell")
    /// Reminder
    internal static let reminder = L10n.tr("Text", "Reminder")
    /// TextCollectionViewCell
    internal static let textCollectionViewCell = L10n.tr("Text", "TextCollectionViewCell")
    /// ThemeCollectionViewCell
    internal static let themeCollectionViewCell = L10n.tr("Text", "ThemeCollectionViewCell")
    /// UnlockAllCollectionViewCell
    internal static let unlockAllCollectionViewCell = L10n.tr("Text", "UnlockAllCollectionViewCell")
    /// Video
    internal static let video = L10n.tr("Text", "Video")
    /// ViewGetStart
    internal static let viewGetStart = L10n.tr("Text", "ViewGetStart")
    /// ViewIntroCategory
    internal static let viewIntroCategory = L10n.tr("Text", "ViewIntroCategory")
    /// ViewIntroIAP
    internal static let viewIntroIAP = L10n.tr("Text", "ViewIntroIAP")
    /// ViewIntroWidget
    internal static let viewIntroWidget = L10n.tr("Text", "ViewIntroWidget")
    /// ViewReminder
    internal static let viewReminder = L10n.tr("Text", "ViewReminder")
    /// Widget
    internal static let widget = L10n.tr("Text", "Widget")
    /// WidgetTableViewCell
    internal static let widgetTableViewCell = L10n.tr("Text", "WidgetTableViewCell")
    /// Your likes
    internal static let yourLikes = L10n.tr("Text", "Your likes")
  }
  internal enum Url {
    /// http://45.118.146.39/
    internal static let base = L10n.tr("Url", "base")
    /// api/getAllCategories
    internal static let getCategory = L10n.tr("Url", "getCategory")
    /// api/getVideosGroupByCategory
    internal static let getCategoryVideos = L10n.tr("Url", "getCategoryVideos")
    /// api/getAllVideos
    internal static let getListVideo = L10n.tr("Url", "getListVideo")
    /// api/getVideosOfCategory/
    internal static let getListVideoOfCate = L10n.tr("Url", "getListVideoOfCate")
    /// %%20
    internal static let spaceCharacter = L10n.tr("Url", "spaceCharacter")
    /// storage/video/
    internal static let videoUrl = L10n.tr("Url", "videoUrl")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
